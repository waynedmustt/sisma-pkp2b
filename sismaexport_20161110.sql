-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sisma
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (4,5,'Rokan Hilir','2016-11-06 21:29:44','2016-11-06 21:29:44');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commodities`
--

DROP TABLE IF EXISTS `commodities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commodities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `unit` varchar(60) NOT NULL,
  `gambar` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commodities`
--

LOCK TABLES `commodities` WRITE;
/*!40000 ALTER TABLE `commodities` DISABLE KEYS */;
INSERT INTO `commodities` VALUES (7,'Beras Segudang','Rp/Kg','/commodities/download.jpg','2016-11-06 21:32:09','2016-11-09 15:36:49'),(8,'Beras Sekuning','Rp/Kg','','2016-11-06 21:33:21','2016-11-06 21:33:21'),(9,'Kedelai Lokal','Rp/Kg','','2016-11-06 22:00:55','2016-11-06 22:00:55'),(10,'Kedelai Impor','Rp/Kg','','2016-11-06 22:01:18','2016-11-06 22:01:18'),(11,'Gula Pasir Curah','Rp/Kg','','2016-11-06 22:01:57','2016-11-06 22:01:57'),(12,'Minyak Goreng Kemasan Isi Ulang \" Bimoli\" Ukuran 1 L','Rp/1 L','','2016-11-06 22:02:37','2016-11-06 22:02:37'),(13,'Minyak Goreng Kemasan Isi Ulang \" Bimoli\" Ukuran 2 L','Rp/2 L','','2016-11-06 22:03:14','2016-11-06 22:03:14'),(14,'Minyak Goreng Curah','Rp/Kg','','2016-11-06 22:03:49','2016-11-06 22:03:49'),(15,'Daging Sapi','Rp/Kg','','2016-11-06 22:04:32','2016-11-06 22:04:32'),(16,'Daging Ayam Broiler','Rp/Kg','','2016-11-06 22:06:04','2016-11-06 22:06:04'),(17,'Telur Ayam Ras','Rp/Btr','','2016-11-06 22:07:03','2016-11-06 22:07:03'),(18,'Cabe Merah Keriting','Rp/Kg','','2016-11-06 22:08:16','2016-11-06 22:08:16');
/*!40000 ALTER TABLE `commodities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commodity_prices`
--

DROP TABLE IF EXISTS `commodity_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commodity_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `price_a` int(60) NOT NULL,
  `price_b` int(11) NOT NULL,
  `survey_location_id` int(11) NOT NULL,
  `survey_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commodity_prices`
--

LOCK TABLES `commodity_prices` WRITE;
/*!40000 ALTER TABLE `commodity_prices` DISABLE KEYS */;
INSERT INTO `commodity_prices` VALUES (10,7,5,4,5,12000,12000,2,'2016-01-21 17:00:00','2016-11-07 19:22:08','2016-11-07 19:22:08'),(11,7,5,4,5,11000,11000,2,'2016-01-26 17:00:00','2016-11-07 19:23:03','2016-11-07 19:23:03'),(12,7,5,4,5,11000,10999,2,'2016-02-03 17:00:00','2016-11-07 19:24:08','2016-11-07 19:24:08'),(13,7,5,4,5,11000,10500,2,'2016-02-09 17:00:00','2016-11-07 19:30:50','2016-11-07 19:30:50');
/*!40000 ALTER TABLE `commodity_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributors`
--

DROP TABLE IF EXISTS `distributors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributors`
--

LOCK TABLES `distributors` WRITE;
/*!40000 ALTER TABLE `distributors` DISABLE KEYS */;
INSERT INTO `distributors` VALUES (3,'PUSKUD RIAU','2016-11-08 23:17:54','2016-11-08 23:20:34'),(4,'PT. SARANA AMONGTANI','2016-11-08 23:18:16','2016-11-08 23:20:52'),(5,'CV. MUSTAKIM','2016-11-08 23:19:37','2016-11-08 23:19:37'),(6,'CV. BINA TANI MAKMUR','2016-11-08 23:20:15','2016-11-08 23:20:15'),(7,'PT. ARTHA MULIA GRAHA','2016-11-08 23:21:17','2016-11-08 23:21:17');
/*!40000 ALTER TABLE `distributors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `districts`
--

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` VALUES (5,4,'Kecamatan Bangko','2016-11-06 21:30:18','2016-11-06 21:30:18'),(6,4,'Kecamatan Sinaboi','2016-11-06 21:30:39','2016-11-06 21:30:39'),(7,4,'Kecamatan Rimba Melintang','2016-11-07 18:45:22','2016-11-07 18:45:22');
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fertilizer_quotas`
--

DROP TABLE IF EXISTS `fertilizer_quotas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fertilizer_quotas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fertilizer_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `retailer_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `quota` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fertilizer_quotas`
--

LOCK TABLES `fertilizer_quotas` WRITE;
/*!40000 ALTER TABLE `fertilizer_quotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `fertilizer_quotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fertilizer_realizations`
--

DROP TABLE IF EXISTS `fertilizer_realizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fertilizer_realizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fertilizer_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `retailer_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `start_stock` int(11) NOT NULL,
  `end_stock` int(11) NOT NULL,
  `redemption` int(11) NOT NULL,
  `month` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fertilizer_realizations`
--

LOCK TABLES `fertilizer_realizations` WRITE;
/*!40000 ALTER TABLE `fertilizer_realizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `fertilizer_realizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fertilizer_type`
--

DROP TABLE IF EXISTS `fertilizer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fertilizer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fertilizer_type`
--

LOCK TABLES `fertilizer_type` WRITE;
/*!40000 ALTER TABLE `fertilizer_type` DISABLE KEYS */;
INSERT INTO `fertilizer_type` VALUES (7,'UREA','UREA BERSUBSIDI','2016-11-09 00:51:55','2016-11-09 00:51:55'),(8,'ORGANIK','ORGANIK BERSUBSIDI','2016-11-09 00:52:17','2016-11-09 00:52:17'),(9,'ZA','ZA BERSUBSIDI','2016-11-09 00:52:33','2016-11-09 00:52:33'),(10,'SP-36','SP-36 BERSUBSIDI','2016-11-09 00:52:53','2016-11-09 00:52:53'),(11,'PONSKA','PONSKA BERSUBSIDI','2016-11-09 00:53:09','2016-11-09 00:53:09');
/*!40000 ALTER TABLE `fertilizer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fertilizers`
--

DROP TABLE IF EXISTS `fertilizers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fertilizers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fertilizer_type_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gambar` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fertilizers`
--

LOCK TABLES `fertilizers` WRITE;
/*!40000 ALTER TABLE `fertilizers` DISABLE KEYS */;
INSERT INTO `fertilizers` VALUES (6,7,'Pupuk Urea','','2016-11-09 00:59:31','2016-11-09 00:59:31');
/*!40000 ALTER TABLE `fertilizers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_role_id_idx` (`role_id`),
  KEY `fk_permission_id_idx` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES (5,'Riau','2016-11-06 21:27:20','2016-11-06 21:27:20');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retailers`
--

DROP TABLE IF EXISTS `retailers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retailers`
--

LOCK TABLES `retailers` WRITE;
/*!40000 ALTER TABLE `retailers` DISABLE KEYS */;
INSERT INTO `retailers` VALUES (3,3,'UD Tani Kita','2016-11-08 23:22:19','2016-11-08 23:22:19'),(4,3,'UD Wada Agung II','2016-11-08 23:22:36','2016-11-08 23:22:36'),(5,3,'UD Wada Agung ','2016-11-08 23:22:58','2016-11-08 23:22:58'),(6,3,'UD Wadah Sejahtera','2016-11-08 23:23:15','2016-11-08 23:23:15'),(7,3,'KUD Pelita Jaya','2016-11-08 23:23:39','2016-11-08 23:23:39'),(8,3,'KUD Karya Manunggal','2016-11-09 00:56:11','2016-11-09 00:56:11'),(9,3,'KUD Anugerah','2016-11-09 00:56:27','2016-11-09 00:56:27'),(10,3,'KSU Rohil Agro','2016-11-09 00:56:48','2016-11-09 00:56:48'),(11,3,'UD Agro Tani Makmur','2016-11-09 00:57:05','2016-11-09 00:57:05'),(12,3,'KSU Rohil Tani','2016-11-09 00:57:23','2016-11-09 00:57:23'),(13,3,'Koperasi HKP2','2016-11-09 00:57:45','2016-11-09 00:57:45'),(14,3,'KUD Subur Makmur','2016-11-09 00:58:01','2016-11-09 00:58:01'),(15,3,'Koperasi Bina Sejahtera','2016-11-09 00:58:17','2016-11-09 00:58:17'),(16,3,'UD Pasir Putih Sejahtera','2016-11-09 00:58:39','2016-11-09 00:58:39');
/*!40000 ALTER TABLE `retailers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `description_name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','admin','for admin','2016-09-21 17:00:00',NULL),(2,'staff','staff','for staff','2016-09-21 17:00:00',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_id_idx` (`role_id`),
  KEY `fk_user_id_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (2,2,1,'2016-09-22 06:05:42','2016-09-22 06:05:42'),(4,4,2,'2016-10-29 13:48:21','2016-10-29 13:48:21');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_locations`
--

DROP TABLE IF EXISTS `survey_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_locations`
--

LOCK TABLES `survey_locations` WRITE;
/*!40000 ALTER TABLE `survey_locations` DISABLE KEYS */;
INSERT INTO `survey_locations` VALUES (2,5,'Pasar Datuk Rubiah','2016-10-31 10:01:02','2016-11-07 18:15:01'),(3,6,'Pasar Selasa Sei Nyamuk','2016-10-31 10:01:13','2016-11-07 18:15:24'),(4,5,'Pasar Selasa Jumrah','2016-11-07 18:45:59','2016-11-07 18:45:59');
/*!40000 ALTER TABLE `survey_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'admin','21232f297a57a5a743894a0e4a801fc3',1,'2016-09-22 06:05:42','2016-10-16 05:13:15'),(4,'staff','1253208465b1efa876f982d8a9e73eef',1,'2016-10-29 13:48:21','2016-11-07 20:20:04');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `villages`
--

DROP TABLE IF EXISTS `villages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `villages`
--

LOCK TABLES `villages` WRITE;
/*!40000 ALTER TABLE `villages` DISABLE KEYS */;
/*!40000 ALTER TABLE `villages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `years`
--

DROP TABLE IF EXISTS `years`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_year` varchar(60) NOT NULL,
  `end_year` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `years`
--

LOCK TABLES `years` WRITE;
/*!40000 ALTER TABLE `years` DISABLE KEYS */;
INSERT INTO `years` VALUES (1,'2010','2016','2016-11-09 15:37:23','2016-11-09 15:37:31');
/*!40000 ALTER TABLE `years` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-10  5:40:50
