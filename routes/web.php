<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['title' => 'Sisma Apps - Dashboard']);
})->middleware('auth');

Route::group(['prefix' => 'auth', 'middleware' => 'redirection'], function () {
    Route::get('login', ['uses' => 'Auth\AuthController@getLogin', 'as' => 'auth.login']);
    Route::post('login', ['uses' => 'Auth\AuthController@postLogin', 'as' => 'auth.login']);
});

Route::get('auth/logout', ['uses' => 'Auth\AuthController@getLogout', 'as' => 'auth.logout']);

// district's route
Route::group(['prefix' => 'district', 'middleware' => 'auth'], function () {
    //get district's route
    Route::get('/', ['uses' => 'District\DistrictController@getIndex', 'as' => 'district'])->middleware('not.admin');

    //get district's route
    Route::get('create', ['uses' => 'District\DistrictController@getCreate', 'as' => 'district.create'])->middleware('not.admin');

    //post district's route
    Route::post('create', ['uses' => 'District\DistrictController@postCreate', 'as' => 'district.create'])->middleware('not.admin');

    //get district's route
    Route::get('update/{id}', ['uses' => 'District\DistrictController@getUpdate', 'as' => 'district.update'])->middleware('not.admin');

    //put district's route
    Route::put('update/{id}', ['uses' => 'District\DistrictController@postUpdate', 'as' => 'district.update'])->middleware('not.admin');

    //delete district's route
    Route::get('delete/{id}', ['uses' => 'District\DistrictController@getDelete', 'as' => 'district.delete'])->middleware('not.admin');

});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
    //get user's route
    Route::get('/', ['uses' => 'User\UserController@getIndex', 'as' => 'user'])->middleware('not.admin');

    //get user's route
    Route::get('create', ['uses' => 'User\UserController@getCreate', 'as' => 'user.create'])->middleware('not.admin');

    //post user's route
    Route::post('create', ['uses' => 'User\UserController@postCreate', 'as' => 'user.create'])->middleware('not.admin');

    //get user's route
    Route::get('update/{id}', ['uses' => 'User\UserController@getUpdate', 'as' => 'user.update'])->middleware('not.admin');

    //put user's route
    Route::put('update/{id}', ['uses' => 'User\UserController@postUpdate', 'as' => 'user.update'])->middleware('not.admin');

    //delete user's route
    Route::get('delete/{id}', ['uses' => 'User\UserController@getDelete', 'as' => 'user.delete'])->middleware('not.admin');

    //status user's route
    Route::get('status/{status}/{id}', ['uses' => 'User\UserController@updateStatus', 'as' => 'user.status'])->middleware('not.admin');
});

//role's route
Route::group(['prefix' => 'role', 'middleware' => 'auth'], function() {

    //get role's route
    Route::get('/', ['uses' => 'Role\RoleController@getIndex', 'as' => 'role'])->middleware('not.admin');

    //get role's route
    Route::get('create', ['uses' => 'Role\RoleController@getCreate', 'as' => 'role.create'])->middleware('not.admin');

    //post role's route
    Route::post('create', ['uses' => 'Role\RoleController@postCreate', 'as' => 'role.create'])->middleware('not.admin');

    //get role's route
    Route::get('update/{id}', ['uses' => 'Role\RoleController@getUpdate', 'as' => 'role.update'])->middleware('not.admin');

    //put role's route
    Route::put('update/{id}', ['uses' => 'Role\RoleController@postUpdate', 'as' => 'role.update'])->middleware('not.admin');

    //delete role's route
    Route::get('delete/{id}', ['uses' => 'Role\RoleController@getDelete', 'as' => 'role.delete'])->middleware('not.admin');

});

//year's route
Route::group(['prefix' => 'year', 'middleware' => 'auth'], function() {

    //get year's route
    Route::get('{id?}', ['uses' => 'Year\YearController@getIndex', 'as' => 'year'])->middleware('not.admin');

    //post year's route
    Route::post('{id?}', ['uses' => 'Year\YearController@postIndex', 'as' => 'year'])->middleware('not.admin');

});

//kebutuhan pokok's route
Route::group(['prefix' => 'kebutuhan-pokok', 'middleware' => 'auth'], function() {

    //get kebutuhan pokok's route
    Route::get('/', ['uses' => 'KebutuhanPokok\KebutuhanPokokController@getIndex', 'as' => 'kebutuhan-pokok']);

    //commodity's route
    Route::group(['prefix' => 'commodity'], function() {

        //get commodity's route
        Route::get('/', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@getIndex', 'as' => 'commodity'])->middleware('not.admin');

        //get commodity's route
        Route::get('create', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@getCreate', 'as' => 'commodity.create'])->middleware('not.admin');

        //post commodity's route
        Route::post('create', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@postCreate', 'as' => 'commodity.create'])->middleware('not.admin');

        //get commodity's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@getUpdate', 'as' => 'commodity.update'])->middleware('not.admin');

        //put commodity's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@postUpdate', 'as' => 'commodity.update'])->middleware('not.admin');

        //delete commodity's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\Commodity\CommodityController@getDelete', 'as' => 'commodity.delete'])->middleware('not.admin');

    });

    // province's route
    Route::group(['prefix' => 'province'], function () {
        //get province's route
        Route::get('/', ['uses' => 'KebutuhanPokok\Province\ProvinceController@getIndex', 'as' => 'province'])->middleware('not.admin');

        //get province's route
        Route::get('create', ['uses' => 'KebutuhanPokok\Province\ProvinceController@getCreate', 'as' => 'province.create'])->middleware('not.admin');

        //post province's route
        Route::post('create', ['uses' => 'KebutuhanPokok\Province\ProvinceController@postCreate', 'as' => 'province.create'])->middleware('not.admin');

        //get province's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\Province\ProvinceController@getUpdate', 'as' => 'province.update'])->middleware('not.admin');

        //put province's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\Province\ProvinceController@postUpdate', 'as' => 'province.update'])->middleware('not.admin');

        //delete province's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\Province\ProvinceController@getDelete', 'as' => 'province.delete'])->middleware('not.admin');

    });

    // city's route
    Route::group(['prefix' => 'city'], function () {
        //get city's route
        Route::get('/', ['uses' => 'KebutuhanPokok\City\CityController@getIndex', 'as' => 'city'])->middleware('not.admin');

        //get city's route
        Route::get('create', ['uses' => 'KebutuhanPokok\City\CityController@getCreate', 'as' => 'city.create'])->middleware('not.admin');

        //post city's route
        Route::post('create', ['uses' => 'KebutuhanPokok\City\CityController@postCreate', 'as' => 'city.create'])->middleware('not.admin');

        //get city's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\City\CityController@getUpdate', 'as' => 'city.update'])->middleware('not.admin');

        //put city's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\City\CityController@postUpdate', 'as' => 'city.update'])->middleware('not.admin');

        //delete city's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\City\CityController@getDelete', 'as' => 'city.delete'])->middleware('not.admin');

    });

    // survey location's route
    Route::group(['prefix' => 'survey-location'], function () {
        //get survey location's route
        Route::get('/', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@getIndex', 'as' => 'survey-location']);

        //get survey location's route
        Route::get('create', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@getCreate', 'as' => 'survey-location.create'])->middleware('not.admin');

        //post survey location's route
        Route::post('create', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@postCreate', 'as' => 'survey-location.create'])->middleware('not.admin');

        //get survey location's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@getUpdate', 'as' => 'survey-location.update'])->middleware('not.admin');

        //put survey location's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@postUpdate', 'as' => 'survey-location.update'])->middleware('not.admin');

        //delete survey location's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\SurveyLocation\SurveyLocationController@getDelete', 'as' => 'survey-location.delete'])->middleware('not.admin');

    });

    //commodity price's route
    Route::group(['prefix' => 'commodity-price'], function() {

        //get commodity price's route
        Route::get('/', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@getIndex', 'as' => 'commodity-price'])->middleware('not.admin');

        //get commodity price's route
        Route::get('create', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@getCreate', 'as' => 'commodity-price.create'])->middleware('not.admin');

        //post commodity price's route
        Route::post('create', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@postCreate', 'as' => 'commodity-price.create'])->middleware('not.admin');

        //get commodity price's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@getUpdate', 'as' => 'commodity-price.update'])->middleware('not.admin');

        //put commodity price's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@postUpdate', 'as' => 'commodity-price.update'])->middleware('not.admin');

        //delete commodity price's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\CommodityPrice\CommodityPriceController@getDelete', 'as' => 'commodity-price.delete'])->middleware('not.admin');

    });

    //commodity stock's route
    Route::group(['prefix' => 'commodity-stock'], function() {

        //get commodity stock's route
        Route::get('/', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@getIndex', 'as' => 'commodity-stock'])->middleware('not.admin');

        //get commodity stock's route
        Route::get('create', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@getCreate', 'as' => 'commodity-stock.create'])->middleware('not.admin');

        //post commodity stock's route
        Route::post('create', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@postCreate', 'as' => 'commodity-stock.create'])->middleware('not.admin');

        //get commodity stock's route
        Route::get('update/{id}', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@getUpdate', 'as' => 'commodity-stock.update'])->middleware('not.admin');

        //put commodity stock's route
        Route::put('update/{id}', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@postUpdate', 'as' => 'commodity-stock.update'])->middleware('not.admin');

        //delete commodity stock's route
        Route::get('delete/{id}', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@getDelete', 'as' => 'commodity-stock.delete'])->middleware('not.admin');

        //report commodity stock's route
        Route::get('report', ['uses' => 'KebutuhanPokok\CommodityStock\CommodityStockController@getReport', 'as' => 'commodity-stock.report']);
    });

    //graph's route
    Route::group(['prefix' => 'graph'], function() {

        //get graph's route
        Route::get('/', ['uses' => 'KebutuhanPokok\Graph\GraphController@getIndex', 'as' => 'graph']);

        //get graph print's route
        Route::get('print/{province}/{city}/{district}/{surveyLocation}/{surveyDate}', ['uses' => 'KebutuhanPokok\Graph\GraphController@getPrint', 'as' => 'graph.print']);

        //get commodity weekly's route
        Route::get('commodity-weekly', ['uses' => 'KebutuhanPokok\Graph\GraphController@getCommodityWeekly', 'as' => 'graph.commodity.weekly']);

        //get commodity graph's route
        Route::get('commodity', ['uses' => 'KebutuhanPokok\Graph\GraphController@getCommodity', 'as' => 'graph.commodity']);

    });

    //price coefficient's route
    Route::group(['prefix' => 'price-coefficient'], function() {

        //get price coefficient's route
        Route::get('/', ['uses' => 'KebutuhanPokok\PriceCoefficient\PriceCoefficientController@getIndex', 'as' => 'price-coefficient']);

        //get print price coefficient's route
        Route::get('print/{surveyLocationId}/{yearSelected}', ['uses' => 'KebutuhanPokok\PriceCoefficient\PriceCoefficientController@getPrint', 'as' => 'price-coefficient.print']);

    });

});

//pupuk bersubsidi's route
Route::group(['prefix' => 'pupuk-bersubsidi', 'middleware' => 'auth'], function() {

    //get pupuk bersubsidi's route
    Route::get('/', ['uses' => 'PupukBersubsidi\PupukBersubsidiController@getIndex', 'as' => 'pupuk-bersubsidi']);

    //fertilizer type's route
    Route::group(['prefix' => 'fertilizer-type'], function() {

        //get fertilizer type's route
        Route::get('/', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@getIndex', 'as' => 'fertilizer-type'])->middleware('not.admin');

        //get fertilizer type's route
        Route::get('create', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@getCreate', 'as' => 'fertilizer-type.create'])->middleware('not.admin');

        //post fertilizer type's route
        Route::post('create', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@postCreate', 'as' => 'fertilizer-type.create'])->middleware('not.admin');

        //get fertilizer type's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@getUpdate', 'as' => 'fertilizer-type.update'])->middleware('not.admin');

        //put fertilizer type's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@postUpdate', 'as' => 'fertilizer-type.update'])->middleware('not.admin');

        //delete fertilizer type's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\FertilizerType\FertilizerTypeController@getDelete', 'as' => 'fertilizer-type.delete'])->middleware('not.admin');

    });

    //fertilizer quota's route
    Route::group(['prefix' => 'fertilizer-quota'], function() {

        //get fertilizer quota's route
        Route::get('/', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@getIndex', 'as' => 'fertilizer-quota'])->middleware('not.admin');

        //get fertilizer quota's route
        Route::get('create', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@getCreate', 'as' => 'fertilizer-quota.create'])->middleware('not.admin');

        //post fertilizer quota's route
        Route::post('create', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@postCreate', 'as' => 'fertilizer-quota.create'])->middleware('not.admin');

        //get fertilizer quota's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@getUpdate', 'as' => 'fertilizer-quota.update'])->middleware('not.admin');

        //put fertilizer quota's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@postUpdate', 'as' => 'fertilizer-quota.update'])->middleware('not.admin');

        //delete fertilizer quota's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\FertilizerQuota\FertilizerQuotaController@getDelete', 'as' => 'fertilizer-quota.delete'])->middleware('not.admin');

    });

    //fertilizer realization's route
    Route::group(['prefix' => 'fertilizer-realization'], function() {

        //get fertilizer realization's route
        Route::get('/', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@getIndex', 'as' => 'fertilizer-realization'])->middleware('not.admin');

        //get fertilizer realization's route
        Route::get('create', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@getCreate', 'as' => 'fertilizer-realization.create'])->middleware('not.admin');

        //post fertilizer realization's route
        Route::post('create', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@postCreate', 'as' => 'fertilizer-realization.create'])->middleware('not.admin');

        //get fertilizer realization's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@getUpdate', 'as' => 'fertilizer-realization.update'])->middleware('not.admin');

        //put fertilizer realization's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@postUpdate', 'as' => 'fertilizer-realization.update'])->middleware('not.admin');

        //delete fertilizer realization's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\FertilizerRealization\FertilizerRealizationController@getDelete', 'as' => 'fertilizer-realization.delete'])->middleware('not.admin');

    });

    //distributor's route
    Route::group(['prefix' => 'distributor'], function() {

        //get distributor's route
        Route::get('/', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@getIndex', 'as' => 'distributor'])->middleware('not.admin');

        //get distributor's route
        Route::get('create', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@getCreate', 'as' => 'distributor.create'])->middleware('not.admin');

        //post distributor's route
        Route::post('create', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@postCreate', 'as' => 'distributor.create'])->middleware('not.admin');

        //get distributor's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@getUpdate', 'as' => 'distributor.update'])->middleware('not.admin');

        //put distributor's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@postUpdate', 'as' => 'distributor.update'])->middleware('not.admin');

        //delete distributor's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\Distributor\DistributorController@getDelete', 'as' => 'distributor.delete'])->middleware('not.admin');

    });

    //retailer's route
    Route::group(['prefix' => 'retailer'], function() {

        //get retailer's route
        Route::get('/', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@getIndex', 'as' => 'retailer'])->middleware('not.admin');

        //get retailer's route
        Route::get('create', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@getCreate', 'as' => 'retailer.create'])->middleware('not.admin');

        //post retailer's route
        Route::post('create', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@postCreate', 'as' => 'retailer.create'])->middleware('not.admin');

        //get retailer's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@getUpdate', 'as' => 'retailer.update'])->middleware('not.admin');

        //put retailer's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@postUpdate', 'as' => 'retailer.update'])->middleware('not.admin');

        //delete retailer's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\Retailer\RetailerController@getDelete', 'as' => 'retailer.delete'])->middleware('not.admin');

    });

    //village's route
    Route::group(['prefix' => 'village'], function() {

        //get village's route
        Route::get('/', ['uses' => 'PupukBersubsidi\Village\VillageController@getIndex', 'as' => 'village'])->middleware('not.admin');

        //get village's route
        Route::get('create', ['uses' => 'PupukBersubsidi\Village\VillageController@getCreate', 'as' => 'village.create'])->middleware('not.admin');

        //post village's route
        Route::post('create', ['uses' => 'PupukBersubsidi\Village\VillageController@postCreate', 'as' => 'village.create'])->middleware('not.admin');

        //get village's route
        Route::get('update/{id}', ['uses' => 'PupukBersubsidi\Village\VillageController@getUpdate', 'as' => 'village.update'])->middleware('not.admin');

        //put village's route
        Route::put('update/{id}', ['uses' => 'PupukBersubsidi\Village\VillageController@postUpdate', 'as' => 'village.update'])->middleware('not.admin');

        //delete village's route
        Route::get('delete/{id}', ['uses' => 'PupukBersubsidi\Village\VillageController@getDelete', 'as' => 'village.delete'])->middleware('not.admin');

    });

    //fertilizer report's route
    Route::group(['prefix' => 'fertilizer-report'], function() {

        //get fertilizer report's route
        Route::get('distributor', ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@getIndex', 'as' => 'fertilizer-report']);

        //get fertilizer report's route
        Route::get('distributor/print/{distributorId}/{fertilizerTypeId}/{monthId}/{yearSelected}',
            ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@printFertilizerReport',
                'as' => 'fertilizer-report.print']);

        //get distributor total's route
        Route::get('distributor-total', ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@getDistributor', 'as' => 'fertilizer-report.distribution']);

        //get distributor total's route
        Route::get('distributor-total/print/{distributorSelected}/{yearSelected}',
            ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@printDistributorTotal',
                'as' => 'fertilizer-report.distribution.print']);

        //get fertilizer distribution's route
        Route::get('fertilizer-distribution', ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@getFertilizerDistribution', 'as' => 'fertilizer-distribution']);

        //print fertilizer distribution's route
        Route::get('fertilizer-distribution/print/{param}',
            ['uses' => 'PupukBersubsidi\FertilizerReport\FertilizerReportController@printFertilizerDistribution',
                'as' => 'fertilizer-distribution.print']);
    });

});


