/**
 * Created by dmustt on 30/10/16.
 */

"use strict";

dmustt.controller('CommodityPriceFormController', [ '$scope', function ($scope) {

    $scope.province = '';
    $scope.provinces = [];
    $scope.city = '';
    $scope.cities = [];
    $scope.district = '';
    $scope.districts = [];
    $scope.surveyLocation = '';
    $scope.surveyLocations = [];
    $scope.commodity = '';
    $scope.commodities = [];
    $scope.commodityPrices = [];
    $scope.priceA = '';
    $scope.priceB = '';
    $scope.surveyDate = '';

    $scope.init = function (commodity, province, city, district, commodityPrices) {
        if (commodity === 'undefined' || commodity === null
            ||province === 'undefined' || province === null) {
            return;
        }

        if (commodityPrices !== '') {
            if (commodityPrices === 'undefined') {
                return;
            }

            angular.forEach(commodityPrices, function(value) {
                $scope.commodityPrices.push({
                    commodity : value.commodity,
                    province : value.province,
                    city : value.city,
                    district : value.district,
                    surveyLocation : value.surveyLocation,
                    priceA : value.priceA,
                    priceB : value.priceB,
                    surveyDate : value.surveyDate
                });
            });
        }

        $scope.provinces = province;
        $scope.cities = city;
        $scope.province = province[0];
        $scope.city = city[0];
        $scope.commodities = commodity;
        $scope.setDistrictByCityId(district);

    };

    $scope.removeCommodityPrice = function (index) {
        $scope.commodityPrices.splice(index, 1);
    };

    $scope.setTimeStampToServer = function (date) {
        if (date === 'undefined') {
            return;
        }

        var convertedDate = new Date(date),
        convertedMonth = convertedDate.getMonth() + 1;

        return convertedDate.getFullYear()
            + '-' + convertedMonth
            + '-' + convertedDate.getDate();
    };

    $scope.addCommodityPrice = function () {
        $scope.commodityPrices.push({
            commodity : $scope.commodity,
            province : $scope.province,
            city : $scope.city,
            district : $scope.district,
            surveyLocation : $scope.surveyLocation,
            priceA : $scope.priceA,
            priceB : $scope.priceB,
            surveyDate : $scope.setTimeStampToServer($scope.surveyDate)
        });
    };

    $scope.setCitiesEmpty = function () {
        $scope.cities = [];
    };

    $scope.setDistrictsEmpty = function () {
        $scope.districts = [];
    };

    $scope.setSurveyLocationsEmpty = function () {
        $scope.surveyLocations = [];
    };

    $scope.setCityByProvinceId = function (city) {
        $scope.setCitiesEmpty();

        if ($scope.provinces === '') {
            return;
        }

        angular.forEach(city, function(value) {
           if (value.province_id == $scope.province.id) {
                $scope.cities.push(value);
           }
        });
    };

    $scope.setDistrictByCityId = function (district) {
        $scope.setDistrictsEmpty();

        if ($scope.districts === '') {
            return;
        }

        angular.forEach(district, function(value) {
            if (value.city_id == $scope.city.id) {
                $scope.districts.push(value);
            }
        });
    };

    $scope.setSurveyLocationByDistrictId = function (surveyLocation) {
        $scope.setSurveyLocationsEmpty();

        if ($scope.surveyLocations === '') {
            return;
        }

        angular.forEach(surveyLocation, function(value) {
            if (value.district_id == $scope.district.id) {
                $scope.surveyLocations.push(value);
            }
        });
    };

}]);
