/**
 * Created by dmustt on 02/11/16.
 */

"use strict";

dmustt.controller('FertilizerRealizationFormController', [ '$scope', function ($scope) {

    $scope.distributor = '';
    $scope.distributors = [];
    $scope.retailer = '';
    $scope.retailers = [];
    $scope.district = '';
    $scope.districts = [];
    $scope.village = '';
    $scope.villages = [];
    $scope.year = '';
    $scope.month = '';
    $scope.fertilizerRealizations = [];

    $scope.init = function (distributor, district) {
        if (distributor === 'undefined' || distributor === null ||
            district === 'undefined' || district === null) {
            return;
        }

        $scope.distributors = distributor;
        $scope.districts = district;

    }

    $scope.removeFertilizerRealization = function (index) {
        $scope.fertilizerRealizations.splice(index, 1);
    };

    $scope.addFertilizerRealization = function () {

        $scope.fertilizerRealizations.push({
            distributor : $scope.distributor,
            retailer : $scope.retailer,
            year : $scope.year,
            month : $scope.setMonthName($scope.month),
        });
    };

    $scope.setMonthName = function (month) {
        if (month === 'undefined') {
            return;
        }

        var months = [];
        months[0] = "Januari";
        months[1] = "Februari";
        months[2] = "Maret";
        months[3] = "April";
        months[4] = "Mei";
        months[5] = "Juni";
        months[6] = "Juli";
        months[7] = "Agustus";
        months[8] = "September";
        months[9] = "Oktober";
        months[10] = "November";
        months[11] = "Desember";

        return months[parseInt(month)-1];
    };

    $scope.setRetailersEmpty = function () {
        $scope.retailers = [];
    }

    $scope.setVillagesEmpty = function () {
        $scope.villages = [];
    }

    $scope.setRetailerByDistributorId = function (retailer) {
        $scope.setRetailersEmpty();

        if ($scope.retailers === '') {
            return;
        }

        angular.forEach(retailer, function(value) {
            if (value.distributor_id == $scope.distributor.id) {
                $scope.retailers.push(value);
            }
        });
    }

    $scope.setVillageByDistrictId = function (village) {
        $scope.setVillagesEmpty();

        if ($scope.villages === '') {
            return;
        }

        angular.forEach(village, function(value) {
            if (value.district_id == $scope.district.id) {
                $scope.villages.push(value);
            }
        });
    }

}]);
