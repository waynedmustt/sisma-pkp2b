@include('header')
        <!--main content start-->
<section id="main-content" xmlns="http://www.w3.org/1999/html">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        SISMA PK2PB
                    </header>
                    <div class="panel-body">
                        @if (session('success'))
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>{{session('success')}}</strong>
                            </div>
                        @endif
                            <h1>Selamat Datang di Aplikasi SISMA PK2PB</h1>
                            <a href="{{route('kebutuhan-pokok')}}" type="button" class="btn btn-shadow btn-default">Informasi Hasil Pemantauan Barang Kebutuhan Pokok</a>
                            <a href="{{route('pupuk-bersubsidi')}}" type="button" class="btn btn-shadow btn-default">Informasi Realisasi Penyaluran Pupuk Bersubsidi</a>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <div class="panel-body">
                        <img style="width: 10%; margin-right: auto; margin-left: auto; display: block" src="{{config('app.url')}}/img/logo.jpg">
                        <p style="margin-top: 20px"><h4 style="text-align: center"><strong>PEMERINTAH KABUPATEN ROKAN HILIR</strong></h4></p>
                        <p><h4 style="text-align: center"><strong>DINAS PERINDUSTRIAN DAN PERDAGANGAN KABUPATEN ROKAN HILIR</strong></h4></p>
                        <p><h6 style="text-align: center"><strong>Jl. Gedung Nasional No. 41 (0767) 217727 Bagansiapiapi Copyright &copy; 2016</strong></h6></p>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')