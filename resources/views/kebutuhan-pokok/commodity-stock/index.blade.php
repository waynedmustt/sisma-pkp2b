@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Stok Komoditas
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            @if (session('failed'))
                                <div class="alert alert-block alert-danger fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('failed')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('commodity-stock.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Komoditas</th>
                                <th>Satuan</th>
                                <th>Stok</th>
                                <th>Ketahanan Stok (Minggu / Bulan)</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($commodityStocks as $commodityStock)
                                    <tr>
                                        @foreach($commodities as $commodity)
                                            @if($commodity->id == $commodityStock->commodity_id)
                                                <td>{{$commodity->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{$commodityStock->unit}}</td>
                                        <td>{{$commodityStock->stock}}</td>
                                        <td>{{$commodityStock->stock_resilience}} {{$commodityStock->period}}</td>
                                        <td>{{$commodityStock->description}}</td>
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('commodity-stock.update', ['id' => encrypt($commodityStock->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('commodity-stock.delete', ['id' => encrypt($commodityStock->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')