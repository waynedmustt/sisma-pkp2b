@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Stok Komoditsa
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('commodity-stock.update', ['id' => $commodityStock->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Komoditas</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="commodity">
                                            @foreach($commodities as $commodity)
                                                <option value="{{$commodity->id}}" @if($commodityStock->commodity_id == $commodity->id) selected @endif>{{$commodity->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Unit</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="unit" value="{{ $commodityStock->unit }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Stok</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="stock" value="{{ $commodityStock->stock }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Ketahanan Stok</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="stock_resilience" value="{{ $commodityStock->stock_resilience }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Periode</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="period">
                                            @foreach($periods as $period)
                                                <option value="{{$period}}" @if($commodityStock->period == $period) selected @endif>{{$period}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}" @if($i == $commodityStock->year) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Bulan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="month">
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}" @if($month['name'] == $commodityStock->month) selected @endif>{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tanggal Pemantauan</label>
                                    <div class="col-lg-10">
                                        <input type="date" class="form-control" name="survey_date" value="{{ date_format(date_create($commodityStock->survey_date), 'Y-m-d') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Deskripsi</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" style="resize: none" name="description">{{ $commodityStock->description }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('commodity-stock')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('kebutuhan-pokok.footer')