@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Stok Komoditas
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('commodity-stock.report')}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}" @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Bulan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="month">
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}"
                                                        @if($monthSelected == $month['name']) selected @endif>{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tanggal Pemantauan</label>
                                    <div class="col-md-8">
                                        <input type="date" class="form-control" name="survey_date"
                                               value="{{ $surveyDateSelected }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Minggu</label>
                                    <div class="col-md-8">
                                        @if($dateConverted >= 1 && $dateConverted <= 7)
                                            <h4>1</h4>
                                        @endif
                                        @if($dateConverted >= 8 && $dateConverted <= 14)
                                            <h4>2</h4>
                                        @endif
                                        @if($dateConverted >= 15 && $dateConverted <= 21)
                                            <h4>3</h4>
                                        @endif
                                        @if($dateConverted >= 22 && $dateConverted <= 28)
                                            <h4>4</h4>
                                        @endif
                                        @if($dateConverted >= 29 && $dateConverted <= 31)
                                            <h4>5</h4>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Komoditas</th>
                                <th>Satuan</th>
                                <th>Stok</th>
                                <th>Ketahanan Stok (Minggu / Bulan)</th>
                                <th>Keterangan</th>
                                </thead>
                                <tbody>
                                @foreach($commodityStocks as $commodityStock)
                                    <tr>
                                        @foreach($commodities as $commodity)
                                            @if($commodity->id == $commodityStock->commodity_id)
                                                <td>{{$commodity->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{$commodityStock->unit}}</td>
                                        <td>{{$commodityStock->stock}}</td>
                                        <td>{{$commodityStock->stock_resilience}} {{$commodityStock->period}}</td>
                                        <td>{{$commodityStock->description}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')