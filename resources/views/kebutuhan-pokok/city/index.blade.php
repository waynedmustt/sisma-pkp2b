@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Kota
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('city.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($cities as $city)
                                    <tr>
                                        <td>{{$city->name}}</td>
                                        @foreach($provinces as $province)
                                            @if($province->id == $city->province_id)
                                        <td>{{$province->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a class="btn btn-success" href="{{route('city.update', ['id' => encrypt($city->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('city.delete', ['id' => encrypt($city->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')