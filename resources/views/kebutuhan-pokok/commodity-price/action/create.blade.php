@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Harga Komoditas
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="CommodityPriceFormController" ng-init="init({{json_encode($commodities)}}, {{json_encode($provinces)}}, {{json_encode($cities)}}, {{json_encode($districts)}},'')" method="post" action="{{route('commodity-price.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Provinsi</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="province" ng-change="setCityByProvinceId({{json_encode($cities)}})" ng-options="province.name for province in provinces">
                                            <option value="">-- Pilih Provinsi --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="provinsi" ng-value="province.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Kota</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="city" ng-change="setDistrictByCityId({{json_encode($districts)}})" ng-options="city.name for city in cities">
                                            <option value="">-- Pilih Kota --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="kota" ng-value="city.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Kecamatan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="district" ng-change="setSurveyLocationByDistrictId({{json_encode($survey_locations)}})" ng-options="district.name for district in districts">
                                            <option value="">-- Pilih Kecamatan --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="kecamatan" ng-value="district.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Lokasi Pemantauan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="surveyLocation" ng-options="surveyLocation.name for surveyLocation in surveyLocations">
                                            <option value="">-- Pilih Lokasi Pemantauan --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="survey_location" ng-value="surveyLocation.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Sampel A</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" ng-model="priceA" name="price_a" value="{{ old('price_a') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Sampel B</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" ng-model="priceB" name="price_b" value="{{ old('price_b') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tanggal Pemantauan</label>
                                    <div class="col-lg-10">
                                        <input type="date" class="form-control" ng-model="surveyDate" name="survey_date" value="{{ old('survey_date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Komoditas</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="commodity" ng-options="commodity.name for commodity in commodities">
                                            <option value="">-- Pilih Komoditas --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="commodity" ng-value="commodity.id"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addCommodityPrice()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Komoditas</th>
                                    <th>Provinsi</th>
                                    <th>Kota</th>
                                    <th>Kecamatan</th>
                                    <th>Lokasi Pemantauan</th>
                                    <th>Sampel A</th>
                                    <th>Sampel B</th>
                                    <th>Tanggal Pemantauan</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="commodityPrice in commodityPrices">
                                        <td>
                                            <% commodityPrice.commodity.name %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][commodity]" value="<% commodityPrice.commodity.id %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.province.name %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][province]" value="<% commodityPrice.province.id %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.city.name %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][city]" value="<% commodityPrice.city.id %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.district.name %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][district]" value="<% commodityPrice.district.id %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.surveyLocation.name %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][surveyLocation]" value="<% commodityPrice.surveyLocation.id %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.priceA | number %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][priceA]" value="<% commodityPrice.priceA %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.priceB | number %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][priceB]" value="<% commodityPrice.priceB %>"/>
                                        </td>
                                        <td>
                                            <% commodityPrice.surveyDate | date:'dd/MM/yyyy' %>
                                            <input class="form-control" type="hidden" name="commodityPrice[<%$index%>][surveyDate]" value="<% commodityPrice.surveyDate %>"/>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeCommodityPrice($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('commodity-price')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('kebutuhan-pokok.footer')