@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Harga Komoditas
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('commodity-price')}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Bulan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="month">
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}"
                                                        @if($month['id'] == $monthSelected) selected @endif>{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                                @if (session('failed'))
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>{{session('failed')}}</strong>
                                    </div>
                                @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('commodity-price.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Komoditas</th>
                                <th>Provinsi</th>
                                <th>Harga Rata"</th>
                                <th>Lokasi Pemantauan</th>
                                <th>Tanggal Pemantauan</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($commodityPrices as $commodityPrice)
                                    <tr>
                                        @foreach($commodities as $commodity)
                                            @if($commodity->id == $commodityPrice->commodity_id)
                                                <td>{{$commodity->name}}</td>
                                            @endif
                                        @endforeach
                                        @foreach($provinces as $province)
                                            @if($province->id == $commodityPrice->province_id)
                                                <td>{{$province->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{number_format(($commodityPrice->price_a + $commodityPrice->price_b) / 2)}}</td>
                                        @foreach($survey_locations as $survey_location)
                                            @if($survey_location->id == $commodityPrice->survey_location_id)
                                                <td>{{$survey_location->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{date_format(date_create($commodityPrice->survey_date), 'd-m-Y')}}</td>
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('commodity-price.update', ['id' => encrypt($commodityPrice->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('commodity-price.delete', ['id' => encrypt($commodityPrice->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')