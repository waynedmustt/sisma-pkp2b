<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{$title}}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{config('app.url')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{config('app.url')}}/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="{{config('app.url')}}/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="{{config('app.url')}}/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{config('app.url')}}/assets/data-tables/DT_bootstrap.css"/>
    <link href="{{config('app.url')}}/assets/morris.js-0.4.3/morris.css" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
    <link href="{{config('app.url')}}/css/style-responsive.css" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body ng-app="DmusttApps">

<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <div class="sidebar-toggle-box">
            <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
        </div>
        <!--logo start-->
        <a href="/" class="logo">
            <img style="width: 10%;" src="{{config('app.url')}}/img/logo.jpg">
            SISMA<span>PK2PB</span></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
        </div>
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">Hai, {{session('userLoggedIn')}}!</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="{{route('auth.logout')}}"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="/">
                        <i class="fa fa-dashboard"></i>
                        <span>Halaman Utama</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('kebutuhan-pokok')}}">
                        <i class="fa fa-laptop"></i>
                        <span>Beranda</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('survey-location')}}">
                        <i class="fa fa-asterisk"></i>
                        <span>Lokasi Pemantauan</span>
                    </a>
                </li>
                @if(session('userLoggedIn') == 'admin')
                    <li>
                        <a href="{{route('province')}}">
                            <i class="fa fa-flash"></i>
                            <span>Daftar Provinsi</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('city')}}">
                            <i class="fa fa-building-o"></i>
                            <span>Daftar Kota</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('commodity')}}">
                            <i class="fa fa-th-list"></i>
                            <span>Daftar Komoditas</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('commodity-price')}}">
                            <i class="fa fa-dollar"></i>
                            <span>Harga Komoditas</span>
                        </a>
                    </li>
                @endif
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fire"></i>
                        <span>Stok Komoditas</span>
                    </a>
                    <ul class="sub">
                        @if(session('userLoggedIn') == 'admin')
                        <li><a href="{{route('commodity-stock')}}">Tabel</a></li>
                        @endif
                        <li><a href="{{route('commodity-stock.report')}}">Laporan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('price-coefficient')}}">
                        <i class="fa fa-align-center"></i>
                        <span>Koefisien Harga</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('graph')}}">
                        <i class="fa fa-align-center"></i>
                        <span>Perbandingan Harga</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('graph.commodity.weekly')}}">
                        <i class="fa fa-barcode"></i>
                        <span>Harga Mingguan</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('graph.commodity')}}">
                        <i class="fa fa-bar-chart-o"></i>
                        <span>Grafik Komoditas</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->