@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Harga Komoditas Mingguan
                    </header>
                    <div class="panel-heading">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('graph.commodity.weekly')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Lokasi Pemantauan</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="survey_location">
                                            @foreach($surveyLocations as $surveyLocation)
                                                <option value="{{$surveyLocation->id}}"
                                                        @if(isset($surveyLocationSelected) && $surveyLocationSelected == $surveyLocation->id) selected @endif>{{$surveyLocation->name}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Bulan</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="month">
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}"
                                                        @if(isset($monthSelected) && $monthSelected == $month['id']) selected @endif>{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Tabel Harga Komoditas Mingguan
                    </header>
                    <div class="panel-body">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#week1">Minggu 1</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#week2">Minggu 2</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#week3">Minggu 3</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#week4">Minggu 4</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#week5">Minggu 5</a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="week1" class="tab-pane active">
                                    <table class="display table table-bordered table-striped">
                                        <thead>
                                        <th>No</th>
                                        <th>Komoditas</th>
                                        <th>Satuan</th>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                        <th>6</th>
                                        <th>7</th>
                                        </thead>
                                        <tbody>
                                        <?php $indexWeek1 = 1;?>
                                        @foreach($commodities as $commodity)
                                            <tr>
                                                <td>{{$indexWeek1}}</td>
                                                <td>{{$commodity->name}}</td>
                                                <td>{{$commodity->unit}}</td>
                                                @for($indexForWeek1 = 1; $indexForWeek1 < 8; $indexForWeek1++)
                                                    <?php $date = str_pad($indexForWeek1, 2, '0', STR_PAD_LEFT) . '-' . $monthSelected . '-' . date('Y');?>
                                                        @if(isset($commodityPriceSelected[$commodity->id][$date]))
                                                        <td>{{number_format($commodityPriceSelected[$commodity->id][$date])}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            <?php $indexWeek1++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="week2" class="tab-pane">
                                    <table class="display table table-bordered table-striped">
                                        <thead>
                                        <th>No</th>
                                        <th>Komoditas</th>
                                        <th>Satuan</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>
                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        </thead>
                                        <tbody>
                                        <?php $indexWeek2 = 1;?>
                                        @foreach($commodities as $commodity)
                                            <tr>
                                                <td>{{$indexWeek2}}</td>
                                                <td>{{$commodity->name}}</td>
                                                <td>{{$commodity->unit}}</td>
                                                @for($indexForWeek2 = 8; $indexForWeek2 < 15; $indexForWeek2++)
                                                    <?php $date = str_pad($indexForWeek2, 2, '0', STR_PAD_LEFT) . '-' . $monthSelected . '-' . date('Y');?>
                                                    @if(isset($commodityPriceSelected[$commodity->id][$date]))
                                                        <td>{{number_format($commodityPriceSelected[$commodity->id][$date])}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            <?php $indexWeek2++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="week3" class="tab-pane">
                                    <table class="display table table-bordered table-striped">
                                        <thead>
                                        <th>No</th>
                                        <th>Komoditas</th>
                                        <th>Satuan</th>
                                        <th>15</th>
                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>
                                        <th>21</th>
                                        </thead>
                                        <tbody>
                                        <?php $indexWeek3 = 1;?>
                                        @foreach($commodities as $commodity)
                                            <tr>
                                                <td>{{$indexWeek3}}</td>
                                                <td>{{$commodity->name}}</td>
                                                <td>{{$commodity->unit}}</td>
                                                @for($indexForWeek3 = 15; $indexForWeek3 < 22; $indexForWeek3++)
                                                    <?php $date = str_pad($indexForWeek3, 2, '0', STR_PAD_LEFT) . '-' . $monthSelected . '-' . date('Y');?>
                                                    @if(isset($commodityPriceSelected[$commodity->id][$date]))
                                                        <td>{{number_format($commodityPriceSelected[$commodity->id][$date])}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            <?php $indexWeek3++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="week4" class="tab-pane">
                                    <table class="display table table-bordered table-striped">
                                        <thead>
                                        <th>No</th>
                                        <th>Komoditas</th>
                                        <th>Satuan</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>
                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        </thead>
                                        <tbody>
                                        <?php $indexWeek4 = 1;?>
                                        @foreach($commodities as $commodity)
                                            <tr>
                                                <td>{{$indexWeek4}}</td>
                                                <td>{{$commodity->name}}</td>
                                                <td>{{$commodity->unit}}</td>
                                                @for($indexForWeek4 = 22; $indexForWeek4 < 29; $indexForWeek4++)
                                                    <?php $date = str_pad($indexForWeek4, 2, '0', STR_PAD_LEFT) . '-' . $monthSelected . '-' . date('Y');?>
                                                    @if(isset($commodityPriceSelected[$commodity->id][$date]))
                                                        <td>{{number_format($commodityPriceSelected[$commodity->id][$date])}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            <?php $indexWeek4++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="week5" class="tab-pane">
                                    <table class="display table table-bordered table-striped">
                                        <thead>
                                        <th>No</th>
                                        <th>Komoditas</th>
                                        <th>Satuan</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        </thead>
                                        <tbody>
                                        <?php $indexWeek5 = 1;?>
                                        @foreach($commodities as $commodity)
                                            <tr>
                                                <td>{{$indexWeek5}}</td>
                                                <td>{{$commodity->name}}</td>
                                                <td>{{$commodity->unit}}</td>
                                                @for($indexForWeek5 = 29; $indexForWeek5 < 32; $indexForWeek5++)
                                                    <?php $date = str_pad($indexForWeek5, 2, '0', STR_PAD_LEFT) . '-' . $monthSelected . '-' . date('Y');?>
                                                    @if(isset($commodityPriceSelected[$commodity->id][$date]))
                                                        <td>{{number_format($commodityPriceSelected[$commodity->id][$date])}}</td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            <?php $indexWeek5++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')