@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Grafik Harga Komoditas Mingguan
                    </header>
                    <div class="panel-heading">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('graph.commodity')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Lokasi Pemantauan</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="survey_location">
                                            @foreach($surveyLocations as $surveyLocation)
                                                <option value="{{$surveyLocation->id}}"
                                                        @if(isset($surveyLocationSelected) && $surveyLocationSelected == $surveyLocation->id) selected @endif>{{$surveyLocation->name}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Komoditas</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="komoditas">
                                            @foreach($commodities as $commodity)
                                                <option value="{{$commodity->id}}"
                                                        @if(isset($commoditySelected) && $commoditySelected == $commodity->id) selected @endif>{{$commodity->name}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        Grafik Komoditas
                    </header>
                    <div class="panel-body">
                        <div id="hero-graph" style="height: 250px;"></div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')