<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>{{$title}}</title>
    <script src="https://use.fontawesome.com/c9e181ee2a.js"></script>
    <style>
        .floating-box {
            float: left;
            width: 150px;
            height: 35px;
        }

        .after-box {
            clear: both;
        }

        .floating-commodities {
            float: left;
            width: 150px;
            height: 250px;
            margin-right: 20px;
            margin-bottom: 20px;
            border: 1px solid #000000;
        }

        .page-break {
            page-break-after: always;
        }

    </style>
</head>
<body>
<h2 style="text-align: center">PERBANDINGAN HARGA KEBUTUHAN POKOK</h2>
<div class="floating-box">Lokasi Pemantauan:</div>
@foreach($survey_locations as $survey_location)
    @if($survey_location->id == $surveyLocationId)
        <div class="floating-box"><strong>{{$survey_location->name}}</strong></div>
    @endif
@endforeach
<div class="after-box"></div>
<div class="floating-box">Tanggal:</div>
<div class="floating-box"><strong>{{date('j F Y', strtotime($dateSelected))}}</strong></div>
<div class="after-box"></div>
@if(isset($results) && !empty($results))
    <?php $index = 0; ?>
    @foreach($commodities as $commodity)
        @if(isset($results[$commodity->id]))
            <div class="floating-commodities">
                <p style="display: inline">
                @if($results[$commodity->id]['price_now'] > $results[$commodity->id]['previous_price'])
                    <i style="font-size: 90px;color: #DE5336;"
                       class="fa fa-arrow-up"><img style="width:50px; height: 30px; margin-top: 10px; margin-left: 10px" src="{{public_path()}}/img/upred.png"></i>
                @endif
                @if($results[$commodity->id]['price_now'] < $results[$commodity->id]['previous_price'])
                    <i style="font-size: 90px;color: #DE5336;"
                       class="fa fa-arrow-down"><img style="width:50px; height: 30px; margin-top: 10px; margin-left: 10px" src="{{public_path()}}/img/downred.png"></i>
                @endif
                @if($results[$commodity->id]['price_now'] == round($results[$commodity->id]['previous_price']))
                    <i style="color: #DE5336;"
                       class="fa fa-arrow-list"><img style="width:50px; height: 30px; margin-top: 10px; margin-left: 10px" src="{{public_path()}}/img/listred.png"></i>
                @endif
                    @if(!empty($commodity->gambar) || $commodity->gambar != NULL)
                        <img style="width:50px; height: 30px; margin-top: 10px; margin-left: 10px" src="{{public_path()}}/uploads{{$commodity->gambar}}">
                    @endif
                </p>
                <p><h5>{{$commodity->name}}</h5></p>
                <p>Harga Sekarang : <strong>{{number_format($results[$commodity->id]['price_now'])}}</strong></p>
                <p>Harga
                    Kemarin: <strong>{{$results[$commodity->id]['previous_price'] != 0 ?
                number_format($results[$commodity->id]['previous_price'])
                : 'Tidak Ada'}}</strong></p>
            </div>
            <?php $index++; ?>
            @if($index != 0 && $index % 6 == 0)
                <div class="after-box"></div>
            @endif
            @if($index != 0 && $index % 12 == 0)
                <div class="page-break"></div>
            @endif
        @endif
    @endforeach
@endif
</body>
</html>