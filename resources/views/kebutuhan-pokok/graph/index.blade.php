@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Perbandingan Harga Komoditas
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">

                            <form class="cmxform form-horizontal tasi-form" ng-controller="CommodityPriceFormController"
                                  ng-init="init({{json_encode($commodities)}}, {{json_encode($provinces)}}, {{json_encode($cities)}}, {{json_encode($districts)}},'')" method="get" action="{{route('graph')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Provinsi</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" ng-model="province"
                                                ng-change="setCityByProvinceId({{json_encode($cities)}})"
                                                ng-options="province.name for province in provinces track by province.id">
                                            <option value="">-- Pilih Provinsi --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="provinsi"
                                               ng-value="province.id"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Kota</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" ng-model="city"
                                                ng-change="setDistrictByCityId({{json_encode($districts)}})"
                                                ng-options="city.name for city in cities track by city.id">
                                            <option value="">-- Pilih Kota --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="kota" ng-value="city.id"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Kecamatan</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" ng-model="district"
                                                ng-change="setSurveyLocationByDistrictId({{json_encode($survey_locations)}})"
                                                ng-options="district.name for district in districts">
                                            <option value="">-- Pilih Kecamatan --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="kecamatan"
                                               ng-value="district.id"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Lokasi Pemantauan</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" ng-model="surveyLocation"
                                                ng-options="surveyLocation.name for surveyLocation in surveyLocations">
                                            <option value="">-- Pilih Lokasi Pemantauan --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="survey_location"
                                               ng-value="surveyLocation.id"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Tanggal Pemantauan</label>
                                    <div class="col-lg-8">
                                        <input type="date" class="form-control" name="survey_date"
                                               value="{{ old('survey_date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                        <a target="_blank" class="btn btn-success" href="{{route('graph.print',
                                        ['province' => $provinceId,
                                        'city' => $cityId,
                                        'district' => $districtId,
                                        'surveyLocation' => $surveyLocationId,
                                        'surveyDate' => $dateSelected])}}">Cetak PDF</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Daftar Komoditas
                        @foreach($survey_locations as $survey_location)
                            @if($survey_location->id == $surveyLocationId)
                                {{$survey_location->name}}
                                @endif
                            @endforeach
                        @if($surveyLocationId != '')
                        {{date('j F Y', strtotime($dateSelected))}}
                        @endif
                    </header>
                    <div class="panel-body">
                        @if(isset($results) && !empty($results))
                            @foreach($commodities as $commodity)
                                @if(isset($results[$commodity->id]))
                            <div class="col-lg-4">
                                <!--widget start-->
                                <aside class="profile-nav alt green-border">
                                    <section class="panel">
                                        <div class="user-heading alt green-bg">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                        @if($results[$commodity->id]['price_now'] > $results[$commodity->id]['previous_price'])
                                                            <i style="font-size: 90px;color: #DE5336;"
                                                               class="fa fa-arrow-up"></i>
                                                        @endif
                                                        @if($results[$commodity->id]['price_now'] < $results[$commodity->id]['previous_price'])
                                                            <i style="font-size: 90px;color: #DE5336;"
                                                               class="fa fa-arrow-down"></i>
                                                        @endif
                                                        @if($results[$commodity->id]['price_now'] == round($results[$commodity->id]['previous_price']))
                                                            <i style="font-size: 90px;color: #DE5336;"
                                                               class="fa fa-arrow-list"></i>
                                                        @endif
                                                </div>
                                                <div class="col-lg-8">
                                                    @if(!empty($commodity->gambar) || $commodity->gambar != NULL)
                                                        <img style="width:150px; height: 100px" src="/uploads{{$commodity->gambar}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                            <h1>{{$commodity->name}}</h1>
                                                </div>
                                            </div>
                                            <p>Harga Sekarang : {{number_format($results[$commodity->id]['price_now'])}}</p>
                                            <p>Harga
                                                Kemarin: {{$results[$commodity->id]['previous_price'] != 0 ?  number_format($results[$commodity->id]['previous_price']) : 'Tidak Ada'}}</p>
                                        </div>
                                    </section>
                                </aside>
                                <!--widget end-->
                            </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('kebutuhan-pokok.footer')