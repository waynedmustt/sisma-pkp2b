@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Komoditas
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('commodity.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Gambar</th>
                                <th>Nama</th>
                                <th>Satuan</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($commodities as $commodity)
                                    <tr>
                                        <td>
                                        @if(!empty($commodity->gambar) || $commodity->gambar != NULL)
                                            <img style="width:20%" src="/uploads{{$commodity->gambar}}">
                                        @else
                                            No Image
                                        @endif
                                        </td>
                                        <td>{{$commodity->name}}</td>
                                        <td>{{$commodity->unit}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('commodity.update', ['id' => encrypt($commodity->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('commodity.delete', ['id' => encrypt($commodity->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')