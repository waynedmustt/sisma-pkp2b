<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>Cetak Koefisien Harga Komoditas</title>
    <style>
        .floating-box {
            float: left;
            width: 150px;
            height: 35px;
        }

        .after-box {
            clear: both;
        }

    </style>
</head>
<body>
<h2 style="text-align: center">KOEFISIEN HARGA KEBUTUHAN POKOK</h2>
<div class="floating-box">Lokasi Pemantauan:</div>
@foreach($surveyLocations as $surveyLocation)
    @if($surveyLocation->id == $surveyLocationId)
        <div class="floating-box"><strong>{{$surveyLocation->name}}</strong></div>
    @endif
@endforeach
<div class="after-box"></div>
<div class="floating-box">Tahun:</div>
<div class="floating-box"><strong>{{$yearSelected}}</strong></div>
<div class="after-box"></div>
<table border="1" style="width: 100%">
    <tr>
        <td rowspan="2"><h5 style="text-align: center">Komoditas</h5></td>
        <td rowspan="2"><h5 style="text-align: center">Satuan</h5></td>
        <td colspan="{{$counter}}"><h5 style="text-align: center">Harga Rataan</h5></td>
            <td rowspan="2"><h5 style="text-align: center">Rata"</h5></td>
            <td rowspan="2"><h5 style="text-align: center">SD</h5></td>
            <td rowspan="2"><h5 style="text-align: center">Harga Tertinggi</h5></td>
            <td rowspan="2"><h5 style="text-align: center">Harga Terendah</h5></td>
            <td rowspan="2"><h5 style="text-align: center">Koefisien Rata-rata</h5></td>
    </tr>
    <tr>
        @foreach($resultDatesFlipped as $resultDateFlipped)
            <td><h5 style="text-align: center">{{date('j F', strtotime($resultDateFlipped))}}</h5></td>
        @endforeach
    </tr>
    <?php $total = 0; $totalKuadrat = 0; $variant = 0; $counterResult = 0; $totalCov = 0; $totalCommodities = 1;?>
    @foreach($commodities as $commodity)
        @if(isset($results[$commodity->id]))
            <tr>
                <td>{{$commodity->name}}</td>
                <td>{{$commodity->unit}}</td>
                @foreach($resultDatesFlipped as $resultDateFlipped)
                    @if(isset($results[$commodity->id][$resultDateFlipped]))
                        <td>{{number_format($results[$commodity->id][$resultDateFlipped])}}</td>
                        <?php $total += $results[$commodity->id][$resultDateFlipped]; $counterResult += 1; ?>
                        <?php $totalKuadrat += ($results[$commodity->id][$resultDateFlipped] * $results[$commodity->id][$resultDateFlipped]); ?>
                    @else
                        <td>0</td>
                    @endif
                @endforeach
                <td>{{number_format(round($total / $counterResult, 2))}}</td>
                <?php
                $variant = 0;
                if ((($counterResult * $totalKuadrat) - ($total * $total)) != 0) {
                    $variant = (($counterResult * $totalKuadrat) - ($total * $total)) / ($counterResult * ($counterResult - 1));
                }
                ?>
                @if($variant != 0)
                    <td>{{number_format(round(sqrt($variant),2))}}</td>
                @else
                    <td>0</td>
                @endif
                <td>{{number_format($results[$commodity->id]['max'])}}</td>
                <td>{{number_format($results[$commodity->id]['min'])}}</td>
                @if($variant != 0)
                    <td>{{round((sqrt($variant) / ($total / $counterResult)) * 100, 2)}}</td>
                    <?php $totalCov += (sqrt($variant) / ($total / $counterResult)) * 100; $totalCommodities += 1; ?>
                @else
                    <td>0</td>
                @endif
            </tr>
        @endif
        <?php $total = 0; $totalKuadrat = 0; $variant = 0; $counterResult = 0;?>
    @endforeach
    <tr>
        <?php $totalRowSpan = 6 + $counter; ?>
        <td colspan="{{$totalRowSpan}}"><center><strong>TOTAL</strong></center></td>
        <td>{{round($totalCov, 2)}}</td>
    </tr>
    <tr>
        <td colspan="{{$totalRowSpan}}"><center><strong>KOEFISIEN RATA - RATA HARGA BAHAN POKOK</strong></center></td>
        <td>{{round($totalCov / $totalCommodities, 2)}}</td>
    </tr>
</table>
</body>
</html>