@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Koefisien Harga Komoditas
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('price-coefficient')}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Lokasi Pemantauan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="surveyLocation">
                                            @foreach($surveyLocations as $surveyLocation)
                                                <option value="{{$surveyLocation->id}}"
                                                        @if($surveyLocation->id == $surveyLocationId) selected @endif>{{$surveyLocation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                        <a target="_blank" class="btn btn-success" href="{{route('price-coefficient.print',
                                        ['surveyLocationId' => $surveyLocationId, 'yearSelected' => $yearSelected])}}">Cetak PDF</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="overflow: scroll;" class="rsp-cal">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <table class="table-bordered" id="example" style="width: 100%">
                                <thead>
                                <th>Komoditas</th>
                                <th>Satuan</th>
                                @foreach($resultDatesFlipped as $resultDateFlipped)
                                    <th>{{date('j F', strtotime($resultDateFlipped))}}</th>
                                @endforeach
                                <th>Rata"</th>
                                <th>SD</th>
                                <th>Harga Tertinggi</th>
                                <th>Harga Terendah</th>
                                <th>Koefisien Variasi Harga (CoV)</th>
                                </thead>
                                <tbody>
                                <?php $total = 0; $counter = 1; $totalKuadrat = 0; $variant = 0; $counterResult = 0; $totalCov = 0;?>
                                @foreach($commodities as $commodity)
                                    @if(isset($results[$commodity->id]))
                                    <tr>
                                        <td>{{$commodity->name}}</td>
                                        <td>{{$commodity->unit}}</td>
                                        @foreach($resultDatesFlipped as $resultDateFlipped)
                                            @if(isset($results[$commodity->id][$resultDateFlipped]))
                                                <td>{{number_format($results[$commodity->id][$resultDateFlipped])}}</td>
                                                <?php $total += $results[$commodity->id][$resultDateFlipped]; $counterResult += 1;?>
                                                <?php $totalKuadrat += ($results[$commodity->id][$resultDateFlipped] * $results[$commodity->id][$resultDateFlipped]); ?>
                                                @else
                                                <td>0</td>
                                            @endif
                                        @endforeach
                                        <td>{{number_format(round($total / $counterResult, 2))}}</td>
                                        <?php
                                            $variant = 0;
                                            if ((($counterResult * $totalKuadrat) - ($total * $total)) != 0) {
                                                $variant = (($counterResult * $totalKuadrat) - ($total * $total)) / ($counterResult * ($counterResult - 1));
                                            }
                                        ?>
                                        @if($variant != 0)
                                        <td>{{number_format(round(sqrt($variant),2))}}</td>
                                            @else
                                            <td>0</td>
                                        @endif
                                        <td>{{number_format($results[$commodity->id]['max'])}}</td>
                                        <td>{{number_format($results[$commodity->id]['min'])}}</td>
                                        @if($variant != 0)
                                        <td>{{round((sqrt($variant) / ($total / $counterResult)) * 100, 2)}}</td>
                                        <?php $totalCov += (sqrt($variant) / ($total / $counterResult)) * 100; $counter += 1; ?>
                                        @else
                                            <td>0</td>
                                        @endif
                                    </tr>
                                    @endif
                                    <?php $total = 0; $totalKuadrat = 0; $variant = 0; $counterResult = 0;?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        KOEFISIEN RATA - RATA
                    </header>
                    <div class="panel-body">
                        {{round($totalCov / $counter, 2)}}
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')