@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Lokasi Pemantauan
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            @if(session('userLoggedIn') == 'admin')
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('survey-location.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            @endif
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Lokasi Pemantauan</th>
                                <th>Kecamatan</th>
                                <th>Keterangan</th>
                                @if(session('userLoggedIn') == 'admin')
                                <th>Action</th>
                                @endif
                                </thead>
                                <tbody>
                                @foreach($surveyLocations as $surveyLocation)
                                    <tr>
                                        <td>{{$surveyLocation->name}}</td>
                                        @foreach($districts as $district)
                                            @if($district->id == $surveyLocation->district_id)
                                                <td>{{$district->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{$surveyLocation->description}}</td>
                                        @if(session('userLoggedIn') == 'admin')
                                        <td>
                                            <a class="btn btn-success" href="{{route('survey-location.update', ['id' => encrypt($surveyLocation->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('survey-location.delete', ['id' => encrypt($surveyLocation->id)])}}">Delete</a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')