@include('kebutuhan-pokok.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Sistem Informasi Hasil Pemantauan Barang Kebutuhan Pokok
                    </header>
                    <div class="panel-body">
                        @if (session('failed'))
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>{{session('failed')}}</strong>
                            </div>
                        @endif
                        <h1>Selamat Datang di Sistem Informasi Hasil Pemantauan Barang Kebutuhan Pokok!</h1>
                    </div>
                </section>
            </div>
        </div>
        {{--<div class="row">--}}
            {{--<div class="col-lg-12">--}}
                {{--<section class="panel">--}}
                    {{--<header class="panel-heading">--}}
                        {{--Daftar Komoditas--}}
                    {{--</header>--}}
                    {{--<div class="panel-body">--}}
                            {{--@foreach($commodities as $commodity)--}}
                                    {{--<div class="col-lg-6">--}}
                                        {{--<!--widget start-->--}}
                                        {{--<aside class="profile-nav alt green-border">--}}
                                            {{--<section class="panel">--}}
                                                {{--<div class="user-heading alt green-bg">--}}
                                                    {{--<div class="row">--}}
                                                        {{--<div class="col-lg-12">--}}
                                                            {{--@if(!empty($commodity->gambar) || $commodity->gambar != NULL)--}}
                                                                {{--<img style="display: block;--}}
                                                                {{--margin-left: auto; margin-right: auto;--}}
                                                                {{--width:150px; height: 100px" src="/uploads{{$commodity->gambar}}">--}}
                                                            {{--@endif--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="row">--}}
                                                        {{--<div class="col-lg-12">--}}
                                                            {{--<span style="font-size: 10px; text-align: center; display: block; margin-top: 5px;">{{$commodity->name}}</span>--}}
                                                            {{--<span style="font-size: 10px; text-align: center; display: block; margin-top: 5px;">Unit: {{$commodity->unit}}</span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</section>--}}
                                        {{--</aside>--}}
                                        {{--<!--widget end-->--}}
                                    {{--</div>--}}
                            {{--@endforeach--}}
                    {{--</div>--}}
                {{--</section>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('kebutuhan-pokok.footer')