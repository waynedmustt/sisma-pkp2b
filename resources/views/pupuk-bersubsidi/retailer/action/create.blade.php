@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Pengecer
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('retailer.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Nama</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Distributor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="distributor">
                                            @foreach($distributors as $distributor)
                                                <option value="{{$distributor->id}}">{{$distributor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Pemilik</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="owner_name" value="{{ old('owner_name') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Alamat</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="address" value="{{ old('address') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">No. HP</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="handphone_number" value="{{ old('handphone_number') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Kecamatan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="district">
                                            @foreach($districts as $district)
                                                <option value="{{$district->id}}">{{$district->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('retailer')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('pupuk-bersubsidi.footer')