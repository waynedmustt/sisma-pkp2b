@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Pengecer
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                                @if (session('failed'))
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>{{session('failed')}}</strong>
                                    </div>
                                @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('retailer.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Pengecer</th>
                                <th>Distributor</th>
                                <th>Tahun</th>
                                <th>Pemilik</th>
                                <th>Alamat</th>
                                <th>NO. HP</th>
                                <th>Kecamatan</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($retailers as $retailer)
                                    <tr>
                                        <td>{{$retailer->name}}</td>
                                        @foreach($distributors as $distributor)
                                            @if($distributor->id == $retailer->distributor_id)
                                        <td>{{$distributor->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{$retailer->year}}</td>
                                        <td>{{$retailer->owner_name}}</td>
                                        <td>{{$retailer->address}}</td>
                                        <td>{{$retailer->handphone_number}}</td>
                                        @foreach($districts as $district)
                                            @if($district->id == $retailer->district_id)
                                                <td>{{$district->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a class="btn btn-success" href="{{route('retailer.update', ['id' => encrypt($retailer->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('retailer.delete', ['id' => encrypt($retailer->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')