@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Jenis Pupuk
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                                @if (session('failed'))
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>{{session('failed')}}</strong>
                                    </div>
                                @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('fertilizer-type.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Gambar</th>
                                <th>Jenis Pupuk</th>
                                <th>Tahun</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($fertilizerTypes as $fertilizerType)
                                    <tr>
                                        <td>
                                            @if(!empty($fertilizerType->gambar) || $fertilizerType->gambar != NULL)
                                                <img style="width:20%" src="/uploads{{$fertilizerType->gambar}}">
                                            @else
                                                No Image
                                            @endif
                                        </td>
                                        <td>{{$fertilizerType->name}}</td>
                                        <td>{{$fertilizerType->year}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('fertilizer-type.update', ['id' => encrypt($fertilizerType->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('fertilizer-type.delete', ['id' => encrypt($fertilizerType->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')