@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Kuota Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                                @if (session('failed'))
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>{{session('failed')}}</strong>
                                    </div>
                                @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('fertilizer-quota.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Jenis Pupuk</th>
                                <th>Kuota</th>
                                <th>HET</th>
                                <th>Satuan</th>
                                <th>Tahun</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($fertilizerQuotas as $fertilizerQuota)
                                    <tr>
                                        @foreach($fertilizerTypes as $fertilizerType)
                                            @if($fertilizerType->id == $fertilizerQuota->fertilizer_type_id)
                                                <td>{{$fertilizerType->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{number_format(round($fertilizerQuota->quota, 2), 1)}}</td>
                                            <td>{{number_format($fertilizerQuota->het)}}</td>
                                        <td>{{$fertilizerQuota->unit}}</td>
                                        <td>{{$fertilizerQuota->year}}</td>
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('fertilizer-quota.update', ['id' => encrypt($fertilizerQuota->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('fertilizer-quota.delete', ['id' => encrypt($fertilizerQuota->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')