@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Desa
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('village.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Desa</th>
                                <th>Kecamatan</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($villages as $village)
                                    <tr>
                                        <td>{{$village->name}}</td>
                                        @foreach($districts as $district)
                                            @if($district->id == $village->district_id)
                                                <td>{{$district->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a class="btn btn-success" href="{{route('village.update', ['id' => encrypt($village->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('village.delete', ['id' => encrypt($village->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')