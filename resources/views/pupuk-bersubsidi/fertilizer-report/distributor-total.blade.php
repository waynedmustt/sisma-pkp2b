@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Tabel Realisasi Total Per Distributor
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('fertilizer-report.distribution')}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Distributor</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="distributor">
                                            @foreach($distributors as $distributor)
                                                <option value="{{$distributor->id}}"
                                                        @if($distributor->id == $distributorSelected) selected @endif>{{$distributor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                        <a target="_blank" class="btn btn-success" href="{{route('fertilizer-report.distribution.print',
                                        ['distributorSelected' => $distributorSelected,
                                        'yearSelected' => $yearSelected])}}">Cetak PDF</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="overflow: scroll;" class="rsp-cal">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <table class="display table table-bordered table-striped" id="example" style="width: 100%">
                                <thead>
                                <th>Bulan</th>
                                @foreach($fertilizerTypes as $fertilizerType)
                                    <th>{{$fertilizerType->name}}</th>
                                @endforeach
                                <th>Total</th>
                                </thead>
                                <tbody>
                                <?php $total = 0; $fertilizerTotals = 0;?>
                                @foreach($months as $month)
                                    <tr>
                                        <td>{{$month['name']}}</td>
                                        @foreach($fertilizerTypes as $fertilizerType)
                                            @if(isset($results[$month['name']][$fertilizerType->id]))
                                                <td>{{$results[$month['name']][$fertilizerType->id]}}</td>
                                                <?php $total = $total + $results[$month['name']][$fertilizerType->id]; ?>
                                            @else
                                                <td>0</td>
                                            @endif
                                        @endforeach
                                        <td>{{$total}}</td>
                                        <?php $fertilizerTotals += $total; ?>
                                    </tr>
                                    <?php $total = 0;?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        TINGKAT REALISASI
                    </header>
                    <div class="panel-body">
                        @if($fertilizerTotals != 0 && $quotaTotal != 0)
                            {{number_format(($fertilizerTotals / $quotaTotal) * 100, 2)}} %
                        @else
                            0 %
                        @endif
                    </div>
                </section>
            </div>
            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        TOTAL KESELURUHAN
                    </header>
                    <div class="panel-body">
                        {{number_format($fertilizerTotals)}}
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')