<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>Cetak Laporan Realisasi Per Distributor</title>
    <style>
        .floating-box {
            float: left;
            width: 150px;
            height: 35px;
        }

        .after-box {
            clear: both;
        }

    </style>
</head>
<body>
<h2 style="text-align: center">TABEL REALISASI TOTAL PER DISTRIBUTOR</h2>
<div class="floating-box">Distributor:</div>
@foreach($distributors as $distributor)
            @if($distributor->id == $distributorSelected)
                <div class="floating-box"><strong>{{$distributor->name}}</strong></div>
            @endif
@endforeach
<div class="after-box"></div>
<div class="floating-box">Tahun:</div>
<div class="floating-box"><strong>{{$yearSelected}}</strong></div>
<div class="after-box"></div>
<table border="1" style="width: 100%">
    <tr>
    <td><strong>Bulan</strong></td>
    @foreach($fertilizerTypes as $fertilizerType)
        <td><strong>{{$fertilizerType->name}}</strong></td>
    @endforeach
    <td><strong>Total</strong></td>
    </tr>
    <?php $total = 0; $fertilizerTotals = 0;?>
    @foreach($months as $month)
        <tr>
            <td>{{$month['name']}}</td>
            @foreach($fertilizerTypes as $fertilizerType)
                @if(isset($results[$month['name']][$fertilizerType->id]))
                    <td>{{$results[$month['name']][$fertilizerType->id]}}</td>
                    <?php $total = $total + $results[$month['name']][$fertilizerType->id]; ?>
                @else
                    <td>0</td>
                @endif
            @endforeach
            <td>{{$total}}</td>
            <?php $fertilizerTotals += $total; ?>
        </tr>
        <?php $total = 0;?>
    @endforeach
    <tr>
        <?php $totalColSpan = 1 + count($fertilizerTypes); ?>
        <td colspan="{{$totalColSpan}}">
            <center><strong>TOTAL</strong></center>
        </td>
        <td>{{number_format($fertilizerTotals)}}</td>
    </tr>
</table>
<br>
<div class="floating-box">Tingkat Realisasi:</div>
@if($fertilizerTotals != 0 && $quotaTotal != 0)
    <div class="floating-box"><strong>{{number_format(($fertilizerTotals / $quotaTotal) * 100, 2)}} %</strong></div>
@else
    <div class="floating-box"><strong>0 %</strong></div>
@endif
</body>
</html>