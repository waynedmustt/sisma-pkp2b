@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Tabel Realisasi Per Distributor Per Jenis Pupuk {{$header}} {{$monthName}} {{$yearSelected}}
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('fertilizer-report', ['param' => 'distributor'])}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Distributor</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="distributor">
                                            @foreach($distributors as $distributor)
                                                <option value="{{$distributor->id}}"
                                                        @if($distributor->id == $distributorId) selected @endif>{{$distributor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Jenis Pupuk</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="fertilizerType">
                                            @foreach($fertilizerTypes as $fertilizerType)
                                                <option value="{{$fertilizerType->id}}"
                                                        @if($fertilizerType->id == $fertilizerTypeId) selected @endif>{{$fertilizerType->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}" @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                        <a target="_blank" class="btn btn-success"
                                           href="{{route('fertilizer-report.print',
                                        ['distributorId' => $distributorId,
                                        'fertilizerTypeId' => $fertilizerTypeId,
                                        'monthId' => $monthId,
                                        'yearSelected' => $yearSelected])}}">Cetak PDF</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="overflow: scroll;" class="rsp-cal">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <table class="display table table-bordered table-striped" id="example" style="width: 100%">
                                <thead>
                                <th>Pengecer</th>
                                <th>Kecamatan</th>
                                @foreach($months as $month)
                                    <th>{{$month['name']}}</th>
                                @endforeach
                                <th>Total</th>
                                </thead>
                                <tbody>
                                <?php $total = 0; $totalAll = 0;?>
                                @foreach($retailers as $retailer)
                                    @if(isset($results[$retailer->id]))
                                    <tr>
                                        <td>{{$retailer->name}}</td>
                                        @foreach($districts as $district)
                                            @if($district->id == $retailer->district_id)
                                                <td>{{$district->name}}</td>
                                            @endif
                                        @endforeach
                                        @foreach($months as $month)
                                            @if(isset($results[$retailer->id][$month['id']]))
                                                <td>{{$results[$retailer->id][$month['id']]}}</td>
                                                <?php $total = $total + $results[$retailer->id][$month['id']]; ?>
                                            @else
                                                <td>0</td>
                                            @endif
                                        @endforeach
                                        <td>{{$total}}</td>
                                        <?php $totalAll += $total;?>
                                    </tr>
                                    @endif
                                    <?php $total = 0;?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        TOTAL KESELURUHAN
                    </header>
                    <div class="panel-body">
                        {{number_format($totalAll)}}
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')