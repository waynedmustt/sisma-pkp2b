<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>Cetak Laporan Realisasi Per Distributor Per Jenis Pupuk</title>
    <style>
        .floating-box {
            float: left;
            width: 150px;
            height: 35px;
        }

        .after-box {
            clear: both;
        }

    </style>
</head>
<body>
<h2 style="text-align: center">TABEL REALISASI PER DISTRIBUTOR PER JENIS PUPUK</h2>
<div class="floating-box">Distributor:</div>
@foreach($distributors as $distributor)
    @if($distributor->id == $distributorId)
        <div class="floating-box"><strong>{{$distributor->name}}</strong></div>
    @endif
@endforeach
<div class="after-box"></div>
<div class="floating-box">Jenis Pupuk:</div>
@foreach($fertilizerTypes as $fertilizerType)
    @if($fertilizerType->id == $fertilizerTypeId)
        <div class="floating-box"><strong>{{$fertilizerType->name}}</strong></div>
    @endif
@endforeach
<div class="after-box"></div>
<div class="floating-box">Tahun:</div>
<div class="floating-box"><strong>{{$yearSelected}}</strong></div>
<div class="after-box"></div>
<table border="1" style="width: 100%">
    <tr>
    <td><strong>Pengecer</strong></td>
    <td><strong>Kecamatan</strong></td>
    @foreach($months as $month)
        <td><strong>{{$month['name']}}</strong></td>
    @endforeach
    <td><strong>Total</strong></td>
    </tr>
    <?php $total = 0; $totalAll = 0;?>
    @foreach($retailers as $retailer)
        @if(isset($results[$retailer->id]))
            <tr>
                <td>{{$retailer->name}}</td>
                @foreach($districts as $district)
                    @if($district->id == $retailer->district_id)
                        <td>{{$district->name}}</td>
                    @endif
                @endforeach
                @foreach($months as $month)
                    @if(isset($results[$retailer->id][$month['id']]))
                        <td>{{$results[$retailer->id][$month['id']]}}</td>
                        <?php $total = $total + $results[$retailer->id][$month['id']]; ?>
                    @else
                        <td>0</td>
                    @endif
                @endforeach
                <td>{{$total}}</td>
                <?php $totalAll += $total; ?>
            </tr>
        @endif
        <?php $total = 0;?>
    @endforeach
    <tr>
        <td colspan="14">
            <center><strong>TOTAL</strong></center>
        </td>
        <td>{{number_format($totalAll)}}</td>
    </tr>
</table>
</body>
</html>