<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>Cetak Laporan Realisasi Penyaluran Pupuk Bersubsidi</title>
    <style>
        .floating-box {
            float: left;
            width: 150px;
            height: 35px;
        }

        .after-box {
            clear: both;
        }

    </style>
</head>
<body>
<h2 style="text-align: center">REALISASI PENYALURAN PUPUK BERSUBSIDI</h2>
<div class="floating-box">Tahun:</div>
<div class="floating-box"><strong>{{$yearSelected}}</strong></div>
<div class="after-box"></div>
<table border="1" style="width: 100%">
    <tr>
    <td><strong>Jenis Pupuk</strong></td>
    @foreach($months as $month)
        <td><strong>{{$month['name']}}</strong></td>
    @endforeach
    <td><strong>Kuota</strong></td>
    <td><strong>Sisa</strong></td>
    </tr>
    <?php $total = 0; $totalQuota = 0; $totalRest = 0; $fertilizerTotal = 0;?>
    @foreach($fertilizerTypes as $fertilizerType)
        <tr>
            <td>{{$fertilizerType->name}}</td>
            @foreach($months as $month)
                @if(isset($results[$fertilizerType->id][$month['name']]))
                    <td>{{number_format($results[$fertilizerType->id][$month['name']])}}</td>
                    <?php $total = $total + $results[$fertilizerType->id][$month['name']]; ?>
                @else
                    <td>0</td>
                @endif
            @endforeach
            <td>{{number_format($fertilizerType->quota)}}</td>
            <td>{{number_format($fertilizerType->quota - $total)}}</td>
        </tr>
        <?php $totalQuota += $fertilizerType->quota;?>
        <?php $totalRest += $fertilizerType->quota - $total;?>
        <?php $fertilizerTotal += $total; ?>
        <?php $total = 0;?>
    @endforeach
    <tr>
        <td colspan="13">
            <center><strong>TOTAL</strong></center>
        </td>
        <td>{{number_format($totalQuota)}}</td>
        <td>{{number_format($totalRest)}}</td>
    </tr>
    <tr>
        <td colspan="13">
            <center><strong>TOTAL REALISASI (PERSEN)</strong></center>
        </td>
        <td colspan="2">
            @if($fertilizerTotal != 0 && $totalQuota != 0)
                {{number_format(($fertilizerTotal / $totalQuota) * 100, 2)}} %
            @else
                0 %
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="13">
            <center><strong>TOTAL REALISASI PENYALURAN (TON)</strong></center>
        </td>
        <td colspan="2">{{number_format($fertilizerTotal)}}</td>
    </tr>
</table>
</body>
</html>