@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                       Tabel Realisasi Total Penyaluran Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="get"
                                  action="{{route('fertilizer-distribution')}}">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}" @if($i == $yearSelected) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit" value="Search"/>
                                        <a target="_blank" class="btn btn-success" href="{{route('fertilizer-distribution.print',
                                        ['param' => $yearSelected])}}">Cetak PDF</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="overflow: scroll;" class="rsp-cal">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <table class="display table table-bordered table-striped" id="example" style="width: 100%">
                                <thead>
                                <th>Jenis Pupuk</th>
                                @foreach($months as $month)
                                    <th>{{$month['name']}}</th>
                                @endforeach
                                <th>Total</th>
                                <th>Kuota</th>
                                <th>Sisa</th>
                                </thead>
                                <tbody>
                                <?php $total = 0; $totalQuota = 0; $totalRest = 0; $fertilizerTotal = 0;?>
                                @foreach($fertilizerTypes as $fertilizerType)
                                    <tr>
                                        <td>{{$fertilizerType->name}}</td>
                                        @foreach($months as $month)
                                            @if(isset($results[$fertilizerType->id][$month['name']]))
                                                <td>{{number_format($results[$fertilizerType->id][$month['name']])}}</td>
                                                <?php $total = $total + $results[$fertilizerType->id][$month['name']]; ?>
                                            @else
                                                <td>0</td>
                                            @endif
                                        @endforeach
                                        <td>{{number_format($total)}}</td>
                                        <td>{{number_format($fertilizerType->quota)}}</td>
                                        <td>{{number_format($fertilizerType->quota - $total)}}</td>
                                    </tr>
                                    <?php $totalQuota += $fertilizerType->quota;?>
                                    <?php $totalRest += $fertilizerType->quota - $total;?>
                                    <?php $fertilizerTotal += $total; ?>
                                    <?php $total = 0;?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">
                        TOTAL REALISASI (PERSEN)
                    </header>
                    <div class="panel-body">
                        @if($fertilizerTotal != 0 && $totalQuota != 0)
                            {{number_format(($fertilizerTotal / $totalQuota) * 100, 2)}} %
                        @else
                            0 %
                        @endif
                    </div>
                </section>
            </div>
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">
                        TOTAL REALISASI PENYALURAN (TON)
                    </header>
                    <div class="panel-body">
                        {{number_format($fertilizerTotal)}}
                    </div>
                </section>
            </div>
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">
                        TOTAL SISA
                    </header>
                    <div class="panel-body">
                        {{number_format($totalRest)}}
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')