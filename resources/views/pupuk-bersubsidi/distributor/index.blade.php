@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Distributor
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                                @if (session('failed'))
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <strong>{{session('failed')}}</strong>
                                    </div>
                                @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('distributor.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Distributor</th>
                                <th>Tahun</th>
                                <th>Alamat</th>
                                <th>Direktur</th>
                                <th>Produsen</th>
                                <th>No. HP</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($distributors as $distributor)
                                    <tr>
                                        <td>{{$distributor->name}}</td>
                                        <td>{{$distributor->year}}</td>
                                        <td>{{$distributor->address}}</td>
                                        <td>{{$distributor->director_name}}</td>
                                        <td>{{$distributor->produsen_name}}</td>
                                        <td>{{$distributor->handphone_number}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('distributor.update', ['id' => encrypt($distributor->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('distributor.delete', ['id' => encrypt($distributor->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')