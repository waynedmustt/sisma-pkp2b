@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Sistem Informasi Realisasi Penyaluran Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        <h1>Selamat Datang di Sistem Informasi Realisasi Penyaluran Pupuk Bersubsidi!</h1>
                    </div>
                </section>
            </div>
        </div>
        {{--<div class="row">--}}
            {{--<div class="col-lg-12">--}}
                {{--<section class="panel">--}}
                    {{--<header class="panel-heading">--}}
                        {{--Daftar Jenis Pupuk--}}
                    {{--</header>--}}
                    {{--<div class="panel-body">--}}
                        {{--@foreach($fertilizerTypes as $fertilizerType)--}}
                            {{--<div class="col-lg-6">--}}
                                {{--<!--widget start-->--}}
                                {{--<aside class="profile-nav alt green-border">--}}
                                    {{--<section class="panel">--}}
                                        {{--<div class="user-heading alt green-bg">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-lg-12">--}}
                                                    {{--@if(!empty($fertilizerType->gambar) || $fertilizerType->gambar != NULL)--}}
                                                        {{--<img style="display: block;--}}
                                                                {{--margin-left: auto; margin-right: auto;--}}
                                                                {{--width:150px; height: 100px" src="/uploads{{$fertilizerType->gambar}}">--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-lg-12">--}}
                                                    {{--<span style="font-size: 10px; text-align: center; display: block; margin-top: 5px;">{{$fertilizerType->name}}</span>--}}
                                                    {{--<span style="font-size: 10px; text-align: center; display: block; margin-top: 5px;">Tahun: {{$fertilizerType->year}}</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</section>--}}
                                {{--</aside>--}}
                                {{--<!--widget end-->--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</section>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')