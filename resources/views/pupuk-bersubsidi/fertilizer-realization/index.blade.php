@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Realisasi Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('fertilizer-realization.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Distributor</th>
                                <th>Pengecer</th>
                                <th>Bulan</th>
                                <th>Tahun</th>
                                @foreach($fertilizerTypes as $fertilizerType)
                                    <th>{{$fertilizerType->name}}</th>
                                @endforeach
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($fertilizerRealizations as $fertilizerRealization)
                                    <tr>
                                        @foreach($distributors as $distributor)
                                            @if($distributor->id == $fertilizerRealization->distributor_id)
                                                <td>{{$distributor->name}}</td>
                                            @endif
                                        @endforeach
                                        @foreach($retailers as $retailer)
                                            @if($retailer->id == $fertilizerRealization->retailer_id)
                                                <td>{{$retailer->name}}</td>
                                            @endif
                                        @endforeach
                                        <td>{{$fertilizerRealization->month}}</td>
                                        <td>{{$fertilizerRealization->year}}</td>
                                        @foreach($fertilizerRealizationStocks as $fertilizerRealizationStock)
                                            @if($fertilizerRealizationStock->fertilizer_realization_id == $fertilizerRealization->id)
                                                <td>{{number_format($fertilizerRealizationStock->stock)}}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('fertilizer-realization.update', ['id' => encrypt($fertilizerRealization->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('fertilizer-realization.delete', ['id' => encrypt($fertilizerRealization->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('pupuk-bersubsidi.footer')