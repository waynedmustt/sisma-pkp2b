@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Realisasi Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form"
                                  ng-controller="FertilizerRealizationFormController"
                                  ng-init="init({{json_encode($distributors)}})" method="post"
                                  action="{{route('fertilizer-realization.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Distributor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="distributor"
                                                ng-change="setRetailerByDistributorId({{json_encode($retailers)}})"
                                                ng-options="distributor.name for distributor in distributors">
                                            <option value="">-- Pilih Distributor --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="distributor"
                                               ng-value="distributor.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Pengecer</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="retailer"
                                                ng-options="retailer.name for retailer in retailers">
                                            <option value="">-- Pilih Pengecer --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="retailer"
                                               ng-value="retailer.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="year" name="year">
                                            <option value="">-- Pilih Tahun --</option>
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Bulan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="month" name="month">
                                            <option value="">-- Pilih Bulan --</option>
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}">{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addFertilizerRealization()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Distributor</th>
                                    <th>Pengecer</th>
                                    <th>Tahun</th>
                                    <th>Bulan</th>
                                    @foreach($fertilizerTypes as $fertilizerType)
                                        <th>{{$fertilizerType->name}}</th>
                                    @endforeach
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="fertilizerRealization in fertilizerRealizations">
                                        <td>
                                            <% fertilizerRealization.distributor.name %>
                                            <input class="form-control" type="hidden" name="fertilizerRealization[<%$index%>][distributor]" value="<% fertilizerRealization.distributor.id %>"/>
                                        </td>
                                        <td>
                                            <% fertilizerRealization.retailer.name %>
                                            <input class="form-control" type="hidden" name="fertilizerRealization[<%$index%>][retailer]" value="<% fertilizerRealization.retailer.id %>"/>
                                        </td>
                                        <td>
                                            <% fertilizerRealization.year %>
                                            <input class="form-control" type="hidden" name="fertilizerRealization[<%$index%>][year]" value="<% fertilizerRealization.year %>"/>
                                        </td>
                                        <td>
                                            <% fertilizerRealization.month %>
                                            <input class="form-control" type="hidden" name="fertilizerRealization[<%$index%>][month]" value="<% fertilizerRealization.month %>"/>
                                        </td>
                                        <?php $indexFertilizerType = 0;?>
                                        @foreach($fertilizerTypes as $fertilizerType)
                                            <td>
                                                <input class="form-control" type="text" name="fertilizerRealization[<%$index%>][stock][{{$indexFertilizerType}}]" placeholder=" Ton ..."/>
                                                <input class="form-control" type="hidden" name="fertilizerRealization[<%$index%>][fertilizerType][{{$indexFertilizerType}}]" value="{{$fertilizerType->id}}"/>
                                            </td>
                                            <?php $indexFertilizerType++; ?>
                                        @endforeach
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeFertilizerRealization($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('fertilizer-realization')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('pupuk-bersubsidi.footer')