@include('pupuk-bersubsidi.header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Realisasi Pupuk Bersubsidi
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="FertilizerRealizationFormController" ng-init="init({{json_encode($distributors)}})" method="post" action="{{route('fertilizer-realization.update', ['id' => $fertilizerRealization->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Distributor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="distributor" ng-change="setRetailerByDistributorId({{json_encode($retailers)}})" ng-options="distributor.name for distributor in distributors">
                                            <option value="">-- Pilih Distributor --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="distributor" ng-value="distributor.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Pengecer</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="retailer" ng-options="retailer.name for retailer in retailers">
                                            <option value="">-- Pilih Pengecer --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="retailer" ng-value="retailer.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="year">
                                            @for($i = $startYear; $i <= $endYear; $i++)
                                                <option value="{{$i}}" @if($i == (int) $fertilizerRealization->year) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Bulan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="month">
                                            @foreach($months as $month)
                                                <option value="{{$month['id']}}" @if($month['name'] == strtolower($fertilizerRealization->month)) selected @endif>{{$month['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label col-md-4">Isi Realisasi</label>
                                </div>
                                <?php $index = 1;?>
                                @foreach($fertilizerTypes as $fertilizerType)
                                    @foreach($fertilizerRealizationStocks as $fertilizerRealizationStock)
                                        @if($fertilizerType->id == $fertilizerRealizationStock->fertilizer_type_id
                                        && $fertilizerRealization->id == $fertilizerRealizationStock->fertilizer_realization_id)
                                    <div class="form-group col-md-6">
                                        <label class="control-label col-md-4">{{$fertilizerType->name}}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" name="stock[{{$index}}]"
                                                   placeholder="Satuan Ton ..."
                                                   value="{{$fertilizerRealizationStock->stock}}"
                                                    />
                                            <input class="form-control" type="hidden" name="fertilizerType[{{$index}}]" value="{{$fertilizerType->id}}"/>
                                        </div>
                                    </div>
                                        @endif
                                    @endforeach
                                    <?php $index++; ?>
                                @endforeach
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('fertilizer-realization')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('pupuk-bersubsidi.footer')