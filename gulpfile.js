const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
        '../../../node_modules/angular/angular.js',
    ], "public/assets/js/main.js")
    .scripts([
        'dmustt/dmustt.angular.js',
        'dmustt/angular/*.js',
        'dmustt/angular/*/*.js'
    ], "public/assets/js/dmustt.js")
});
