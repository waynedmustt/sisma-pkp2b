-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 08 Nov 2016 pada 07.45
-- Versi Server: 5.5.53-0ubuntu0.14.04.1
-- Versi PHP: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `sisma`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `cities`
--

INSERT INTO `cities` (`id`, `province_id`, `name`, `created_at`, `updated_at`) VALUES
(4, 5, 'Rokan Hilir', '2016-11-06 21:29:44', '2016-11-06 21:29:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `commodities`
--

CREATE TABLE IF NOT EXISTS `commodities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `unit` varchar(60) NOT NULL,
  `gambar` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `commodities`
--

INSERT INTO `commodities` (`id`, `name`, `unit`, `gambar`, `created_at`, `updated_at`) VALUES
(7, 'Beras Segudang', 'Rp/Kg', '', '2016-11-06 21:32:09', '2016-11-06 21:32:09'),
(8, 'Beras Sekuning', 'Rp/Kg', '', '2016-11-06 21:33:21', '2016-11-06 21:33:21'),
(9, 'Kedelai Lokal', 'Rp/Kg', '', '2016-11-06 22:00:55', '2016-11-06 22:00:55'),
(10, 'Kedelai Impor', 'Rp/Kg', '', '2016-11-06 22:01:18', '2016-11-06 22:01:18'),
(11, 'Gula Pasir Curah', 'Rp/Kg', '', '2016-11-06 22:01:57', '2016-11-06 22:01:57'),
(12, 'Minyak Goreng Kemasan Isi Ulang " Bimoli" Ukuran 1 L', 'Rp/1 L', '', '2016-11-06 22:02:37', '2016-11-06 22:02:37'),
(13, 'Minyak Goreng Kemasan Isi Ulang " Bimoli" Ukuran 2 L', 'Rp/2 L', '', '2016-11-06 22:03:14', '2016-11-06 22:03:14'),
(14, 'Minyak Goreng Curah', 'Rp/Kg', '', '2016-11-06 22:03:49', '2016-11-06 22:03:49'),
(15, 'Daging Sapi', 'Rp/Kg', '', '2016-11-06 22:04:32', '2016-11-06 22:04:32'),
(16, 'Daging Ayam Broiler', 'Rp/Kg', '', '2016-11-06 22:06:04', '2016-11-06 22:06:04'),
(17, 'Telur Ayam Ras', 'Rp/Btr', '', '2016-11-06 22:07:03', '2016-11-06 22:07:03'),
(18, 'Cabe Merah Keriting', 'Rp/Kg', '', '2016-11-06 22:08:16', '2016-11-06 22:08:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `commodity_prices`
--

CREATE TABLE IF NOT EXISTS `commodity_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `price_a` int(60) NOT NULL,
  `price_b` int(11) NOT NULL,
  `survey_location_id` int(11) NOT NULL,
  `survey_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `distributors`
--

CREATE TABLE IF NOT EXISTS `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `distributors`
--

INSERT INTO `distributors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'PT Makmur', '2016-11-01 23:44:32', '2016-11-06 16:51:09'),
(2, 'PT Petrokimia', '2016-11-01 23:44:32', '2016-11-06 16:51:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `districts`
--

INSERT INTO `districts` (`id`, `city_id`, `name`, `created_at`, `updated_at`) VALUES
(5, 4, 'Kecamatan Bangko', '2016-11-06 21:30:18', '2016-11-06 21:30:18'),
(6, 4, 'Kecamatan Sinaboi', '2016-11-06 21:30:39', '2016-11-06 21:30:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fertilizers`
--

CREATE TABLE IF NOT EXISTS `fertilizers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fertilizer_type_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gambar` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `fertilizers`
--

INSERT INTO `fertilizers` (`id`, `fertilizer_type_id`, `name`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 1, 'pupuk bali', '', '2016-11-01 14:32:56', '2016-11-01 14:32:56'),
(2, 2, 'pupuk jawa', '', '2016-11-01 14:33:05', '2016-11-01 14:33:05'),
(3, 3, 'pupuk sumatera', '', '2016-11-01 14:33:14', '2016-11-01 14:33:14'),
(5, 1, 'pupuk kalimantan', '', '2016-11-01 14:35:18', '2016-11-01 14:39:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fertilizer_quotas`
--

CREATE TABLE IF NOT EXISTS `fertilizer_quotas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fertilizer_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `retailer_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `quota` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `fertilizer_quotas`
--

INSERT INTO `fertilizer_quotas` (`id`, `fertilizer_id`, `distributor_id`, `retailer_id`, `district_id`, `village_id`, `quota`, `date`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, 3, 1, 5670, '2016-11-01 16:00:00', '2016-11-01 15:56:19', '2016-11-01 16:01:30'),
(3, 3, 2, 2, 4, 2, 2500, '2016-11-02 16:00:00', '2016-11-02 01:39:51', '2016-11-02 01:39:51'),
(4, 1, 1, 1, 3, 1, 2600, '2016-11-03 16:00:00', '2016-11-04 06:10:14', '2016-11-04 06:10:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fertilizer_type`
--

CREATE TABLE IF NOT EXISTS `fertilizer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `fertilizer_type`
--

INSERT INTO `fertilizer_type` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'urea', 'for urea', '2016-11-01 22:19:11', '0000-00-00 00:00:00'),
(2, 'organik', 'for organik', '2016-11-01 22:19:32', '0000-00-00 00:00:00'),
(3, 'ZA', 'for ZA', '2016-11-01 22:19:32', '0000-00-00 00:00:00'),
(4, 'poniska', 'for poniska', '2016-11-01 22:19:54', '0000-00-00 00:00:00'),
(5, 'SP-26', 'for SP-26', '2016-11-01 22:19:54', '0000-00-00 00:00:00'),
(6, 'ponik', 'for ponik', '2016-11-01 22:20:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_role_id_idx` (`role_id`),
  KEY `fk_permission_id_idx` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `created_at`, `updated_at`) VALUES
(5, 'Riau', '2016-11-06 21:27:20', '2016-11-06 21:27:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retailers`
--

CREATE TABLE IF NOT EXISTS `retailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `retailers`
--

INSERT INTO `retailers` (`id`, `distributor_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'sukses jaya', '2016-11-01 23:44:55', '0000-00-00 00:00:00'),
(2, 2, 'sukses makmur', '2016-11-01 23:44:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `description_name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `name`, `display_name`, `description_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'for admin', '2016-09-21 17:00:00', NULL),
(2, 'staff', 'staff', 'for staff', '2016-09-21 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_id_idx` (`role_id`),
  KEY `fk_user_id_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(2, 2, 1, '2016-09-22 06:05:42', '2016-09-22 06:05:42'),
(4, 4, 2, '2016-10-29 13:48:21', '2016-10-29 13:48:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `survey_locations`
--

CREATE TABLE IF NOT EXISTS `survey_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `survey_locations`
--

INSERT INTO `survey_locations` (`id`, `district_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 5, 'Pasar Santa', '2016-10-31 10:01:02', '2016-11-07 13:02:25'),
(2, 5, 'Pasar Senen', '2016-10-31 10:01:02', '2016-11-07 13:02:29'),
(3, 6, 'Pasar Dago', '2016-10-31 10:01:13', '2016-11-07 13:02:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2016-09-22 06:05:42', '2016-10-16 05:13:15'),
(4, 'staff', '1253208465b1efa876f982d8a9e73eef', 0, '2016-10-29 13:48:21', '2016-10-29 13:48:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `villages`
--

CREATE TABLE IF NOT EXISTS `villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `villages`
--

INSERT INTO `villages` (`id`, `district_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 3, 'desa makmur', '2016-11-01 23:45:22', '0000-00-00 00:00:00'),
(2, 4, 'desa jaya', '2016-11-01 23:45:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `years`
--

CREATE TABLE IF NOT EXISTS `years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_year` varchar(60) NOT NULL,
  `end_year` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
