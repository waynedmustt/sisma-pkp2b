<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FertilizerRealizationWasCreated
{
    use InteractsWithSockets, SerializesModels;

    public $stock, $fertilizerRealizationId, $fertilizerTypeId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($stock,$fertilizerRealizationId, $fertilizerTypeId)
    {
        $this->stock = $stock;
        $this->fertilizerRealizationId = $fertilizerRealizationId;
        $this->fertilizerTypeId = $fertilizerTypeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
