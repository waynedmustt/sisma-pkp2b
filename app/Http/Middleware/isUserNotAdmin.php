<?php

namespace App\Http\Middleware;

use Closure;

class isUserNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('userLoggedIn') && session('userLoggedIn') != 'admin') {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
