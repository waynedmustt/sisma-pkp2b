<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:17
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock\Actions;


use App\Http\Controllers\KebutuhanPokok\CommodityStock\Requests\CommodityStockRequest;
use App\Http\Models\CommodityStock;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int)$year->start_year;
            $endYear = (int)$year->end_year;

            $data = [
                'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Stok Barang Kebutuhan Pokok',
                'periods' => $this->period,
                'commodities' => $this->commodity,
                'months' => $this->months,
                'startYear' => $startYear,
                'endYear' => $endYear
            ];

            return view('kebutuhan-pokok.commodity-stock.action.create', $data);
        }

        return redirect('kebutuhan-pokok/commodity-stock')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param CommodityStockRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(CommodityStockRequest $request)
    {
        $commodityStockInput = $request->except(['_token', 'submit']);

        $commodityStock = new CommodityStock;

        $commodityStock->commodity_id = $commodityStockInput['commodity'];
        $commodityStock->unit = $commodityStockInput['unit'];
        $commodityStock->stock = (float) $commodityStockInput['stock'];
        $commodityStock->stock_resilience = $commodityStockInput['stock_resilience'];
        $commodityStock->period = $commodityStockInput['period'];
        $commodityStock->year = $commodityStockInput['year'];
        $commodityStock->month = $this->months[$commodityStockInput['month']-1]['name'];
        $commodityStock->survey_date = $commodityStockInput['survey_date'];
        $commodityStock->description = $commodityStockInput['description'];

        $commodityStock->save();

        return redirect('kebutuhan-pokok/commodity-stock')->with('success' , 'Data Recorded!');
    }
}