<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:30
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock\Actions;


use App\Http\Models\CommodityStock;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($CommodityStock = CommodityStock::find($decrypted)) {

                $CommodityStock->delete();

                return redirect('kebutuhan-pokok/commodity-stock')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}