<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:32
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock\Actions;


use App\Http\Controllers\KebutuhanPokok\CommodityStock\Requests\CommodityStockRequest;
use App\Http\Models\CommodityStock;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($year = $this->year->first()) {

                $startYear = (int)$year->start_year;
                $endYear = (int)$year->end_year;

                if ($commodityStock = $this->commodityStock->where('id', $decrypted)->first()) {

                    $data = [
                        'title' => 'Sisma Apps - Perbaharui Stok Komoditas',
                        'commodityStock' => $commodityStock,
                        'commodities' => $this->commodity,
                        'periods' => $this->period,
                        'months' => $this->months,
                        'startYear' => $startYear,
                        'endYear' => $endYear
                    ];
                    return view('kebutuhan-pokok.commodity-stock.action.update', $data);
                }
                return view('errors.404');
            }

            return redirect('kebutuhan-pokok/commodity-stock')->with('failed' , 'Year has not been input!');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param CommodityStockRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CommodityStockRequest $request, $id)
    {
        $commodityStockInput = $request->except(['_token', 'submit']);

        $commodityStock = CommodityStock::find($id);

        $commodityStock->commodity_id = $commodityStockInput['commodity'];
        $commodityStock->unit = $commodityStockInput['unit'];
        $commodityStock->stock = (float) $commodityStockInput['stock'];
        $commodityStock->stock_resilience = $commodityStockInput['stock_resilience'];
        $commodityStock->period = $commodityStockInput['period'];
        $commodityStock->year = $commodityStockInput['year'];
        $commodityStock->month = $this->months[$commodityStockInput['month']-1]['name'];
        $commodityStock->survey_date = $commodityStockInput['survey_date'];
        $commodityStock->description = $commodityStockInput['description'];

        $commodityStock->save();

        return redirect('kebutuhan-pokok/commodity-stock')->with('success' , 'Data Updated!');
    }
}