<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 19:26
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock;


use App\Http\Models\CommodityStock;
use Illuminate\Http\Request;

trait Report
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getReport(Request $request)
    {
        if ($year = $this->year->first()) {
            $startYear = (int)$year->start_year;
            $endYear = (int)$year->end_year;

            $yearSelected = $endYear;
            $monthSelected = $this->months[0]['name'];
            $surveyDateSelected = date('Y-m-d');

            if ($commodityStockInput = $request->input()) {
                $yearSelected = $commodityStockInput['year'];
                $monthSelected = $this->months[$commodityStockInput['month'] - 1]['name'];
                $surveyDateSelected = $commodityStockInput['survey_date'];
            }

            $result = CommodityStock::where('year', $yearSelected)
                ->where('month', $monthSelected)
                ->whereDate('survey_date', '=', $surveyDateSelected)
                ->get();

            $dateConverted = date_format(date_create($surveyDateSelected), 'd');

            $data = [
                'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Stok Barang Kebutuhan Pokok',
                'commodities' => $this->commodity,
                'months' => $this->months,
                'startYear' => $startYear,
                'endYear' => $endYear,
                'commodityStocks' => $result,
                'yearSelected' => $yearSelected,
                'monthSelected' => $monthSelected,
                'surveyDateSelected' => $surveyDateSelected,
                'dateConverted' => $dateConverted
            ];

            return view('kebutuhan-pokok.commodity-stock.report', $data);
        }

        return redirect('kebutuhan-pokok/commodity-stock')->with('failed' , 'Year has not been input!');
    }
}