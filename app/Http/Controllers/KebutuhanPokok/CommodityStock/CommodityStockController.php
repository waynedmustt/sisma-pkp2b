<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:05
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock;


use App\Http\Controllers\Controller;
use App\Http\Models\Commodity;
use App\Http\Models\CommodityStock;
use App\Http\Models\Year;

class CommodityStockController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodityStock, $year,
        $commodity,
        $period = ['minggu', 'bulan'],
        $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];

    use Actions\Create, Actions\Delete, Actions\Update, Report;

    /**
     * CommodityStockController constructor.
     */
    public function __construct()
    {
        $this->commodity = Commodity::all();
        $this->commodityStock = CommodityStock::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Stok Barang Kebutuhan Pokok',
            'commodities' => $this->commodity,
            'commodityStocks' => $this->commodityStock
        ];

        return view('kebutuhan-pokok.commodity-stock.index', $data);
    }
}