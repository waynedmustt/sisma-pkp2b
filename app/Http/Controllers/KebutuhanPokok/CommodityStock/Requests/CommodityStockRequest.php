<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:19
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityStock\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CommodityStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commodity' => 'required',
            'unit' => 'required',
            'stock' => 'required',
            'stock_resilience' => 'required',
            'period' => 'required',
            'survey_date' => 'required',
            'description' => 'required'
        ];
    }
}