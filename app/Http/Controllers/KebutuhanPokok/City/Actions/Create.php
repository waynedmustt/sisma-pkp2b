<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:33
 */

namespace App\Http\Controllers\KebutuhanPokok\City\Actions;


use App\Http\Controllers\KebutuhanPokok\City\Requests\CityRequest;
use App\Http\Models\City;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Kota',
            'provinces' => $this->province
        ];

        return view('kebutuhan-pokok.city.action.create', $data);
    }

    /**
     * @param CityRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(CityRequest $request)
    {
        $city_input = $request->except(['_token', 'submit']);

        $city = new City;

        $city->name = $city_input['name'];
        $city->province_id = $city_input['province'];

        $city->save();

        return redirect('kebutuhan-pokok/city')->with('success' , 'Data Recorded!');
    }
}