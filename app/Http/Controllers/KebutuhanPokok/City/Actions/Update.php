<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:42
 */

namespace App\Http\Controllers\KebutuhanPokok\City\Actions;


use App\Http\Controllers\KebutuhanPokok\City\Requests\CityRequest;
use App\Http\Models\City;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($city = $this->city->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Kota ' . $city->name,
                    'city' => $city,
                    'provinces' => $this->province
                ];

                return view('kebutuhan-pokok.city.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param CityRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CityRequest $request, $id)
    {
        $city_input = $request->except(['_token', 'submit']);

        $city = City::find($id);

        $city->name = $city_input['name'];
        $city->province_id = $city_input['province'];

        $city->save();

        return redirect('kebutuhan-pokok/city')->with('success' , 'Data Updated!');
    }
}