<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:40
 */

namespace App\Http\Controllers\KebutuhanPokok\City\Actions;


use App\Http\Models\City;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($city = City::find($decrypted)) {

                $city->delete();

                return redirect('kebutuhan-pokok/city')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}