<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:27
 */

namespace App\Http\Controllers\KebutuhanPokok\City;


use App\Http\Controllers\Controller;
use App\Http\Models\City;
use App\Http\Models\Province;

class CityController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $city, $province;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * CityController constructor.
     */
    public function __construct()
    {
        $this->city = City::all();
        $this->province = Province::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Kota',
            'provinces' => $this->province,
            'cities' => $this->city
        ];

        return view('kebutuhan-pokok.city.index', $data);
    }
}