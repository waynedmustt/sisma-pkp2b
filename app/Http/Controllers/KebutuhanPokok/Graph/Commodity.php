<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 20:26
 */

namespace App\Http\Controllers\KebutuhanPokok\Graph;


use App\Http\Models\CommodityPrice;
use Illuminate\Http\Request;

trait Commodity
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCommodity(Request $request)
    {

        if (!$this->survey_location || !$this->commodity) {
            return view('errors.404');
        }

        $survey_location_id = $this->survey_location[0]->id;

        $commodity_id = $this->commodity[0]->id;

        if ($commodityGraphRequest = $request->input()) {
            $survey_location_id = $commodityGraphRequest['survey_location'];
            $commodity_id = $commodityGraphRequest['komoditas'];
        }

        $commodityPriceSelected = [];

        $chart_komoditas = collect();

        if (!$commodityPrices = CommodityPrice::where('survey_location_id', $survey_location_id)
        ->where('commodity_id', $commodity_id)
        ->get()) {
            return view('errors.404');
        }

        $commodityPricesGroupBy = $commodityPrices->groupBy('commodity_id');

        foreach($commodityPricesGroupBy as $key => $items){
            foreach($items as $item){

                $date = date_format(date_create($item->survey_date), 'Y-m-d');

                $chart_komoditas->push(['tahun' => $date, 'harga' => ($item->price_a + $item->price_b) / 2])->all();

            }
        }

        if ($chart_komoditas->count() <= 0) {
            $chart_komoditas->push(['tahun' => 'Tidak Ada Tahun', 'harga' => null]);
        }


        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Grafik Komoditas',
            'surveyLocations' => $this->survey_location,
            'surveyLocationSelected' => $survey_location_id,
            'commodities' => $this->commodity,
            'commoditySelected' => $commodity_id,
            'commodityPriceSelected' => $commodityPriceSelected,
            'chart_komoditas' => $chart_komoditas
        ];

        return view('kebutuhan-pokok.graph.commodity', $data);
    }
}