<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 31/10/16
 * Time: 20:17
 */

namespace App\Http\Controllers\KebutuhanPokok\Graph;


use App\Http\Controllers\Controller;
use App\Http\Models\City;
use App\Http\Models\Commodity;
use App\Http\Models\CommodityPrice;
use App\Http\Models\District;
use App\Http\Models\Province;
use App\Http\Models\SurveyLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class GraphController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodity, $province, $city, $district, $survey_location, $commodityPrice;

    use CommodityWeekly, \App\Http\Controllers\KebutuhanPokok\Graph\Commodity;

    /**
     * GraphController constructor.
     */
    public function __construct()
    {
        $this->commodity = Commodity::all();
        $this->province = Province::all();
        $this->city = City::all();
        $this->district = District::all();
        $this->survey_location = SurveyLocation::all();
        $this->commodityPrice = CommodityPrice::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $previousIsNotExist = true;

        $commodityPriceSearches = '';
        $previousCommodityPriceSearches = '';
        $provinceId = '';
        $cityId = '';
        $districtId = '';
        $dateSelected = date('d-m-Y');
        $surveyLocationId = '';

        if ($commodityPriceRequests = $request->input()) {
            $provinceId = $commodityPriceRequests['provinsi'];
            $cityId = $commodityPriceRequests['kota'];
            $districtId = $commodityPriceRequests['kecamatan'];
            $surveyLocationId = $commodityPriceRequests['survey_location'];
            $dateSelected = $commodityPriceRequests['survey_date'];
            $commodityPriceSearches = $this->getCommodityPriceByDate('=', $commodityPriceRequests);
            $previousCommodityPriceSearches = $this->getCommodityPriceByDate('<', $commodityPriceRequests);

        }

        $result = $this->setCommodityPriceResult($commodityPriceSearches,
            $previousCommodityPriceSearches,
            $previousIsNotExist);


        $data = [
            'title' => 'Tampil Perbandingan Harga Komoditas',
            'provinces' => $this->province,
            'cities' => $this->city,
            'districts' => $this->district,
            'provinceId' => $provinceId,
            'cityId' => $cityId,
            'districtId' => $districtId,
            'surveyLocationId' => $surveyLocationId,
            'dateSelected' => $dateSelected,
            'survey_locations' => $this->survey_location,
            'results' => $result,
            'commodities' => $this->commodity
        ];

        return view('kebutuhan-pokok.graph.index', $data);
    }

    /**
     * @param $province
     * @param $city
     * @param $district
     * @param $surveyLocation
     * @param $surveyDate
     * @return mixed
     */
    public function getPrint($province, $city, $district, $surveyLocation, $surveyDate)
    {
        $commodityPriceRequests = [
            'provinsi' => $province,
            'kota' => $city,
            'kecamatan' => $district,
            'survey_location' => $surveyLocation,
            'survey_date' => $surveyDate
        ];

        $commodityPriceSearches = $this->getCommodityPriceByDate('=', $commodityPriceRequests);
        $previousCommodityPriceSearches = $this->getCommodityPriceByDate('<', $commodityPriceRequests);

        $result = $this->setCommodityPriceResult($commodityPriceSearches, $previousCommodityPriceSearches,
            true);

        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Cetak Perbandingan Harga Komoditas',
            'provinces' => $this->province,
            'cities' => $this->city,
            'districts' => $this->district,
            'provinceId' => $province,
            'cityId' => $city,
            'districtId' => $district,
            'surveyLocationId' => $surveyLocation,
            'dateSelected' => $surveyDate,
            'survey_locations' => $this->survey_location,
            'results' => $result,
            'commodities' => $this->commodity
        ];

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('kebutuhan-pokok.graph.print-graph', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();

    }

    /**
     * @param $comparison
     * @param $request
     * @return mixed
     */
    private function getCommodityPriceByDate($comparison, $request)
    {
        $dateNow = date_format(date_create($request['survey_date']), 'Y-m-d');

        $commodityPrice = CommodityPrice::where('province_id', $request['provinsi'])
            ->where('city_id', $request['kota'])
            ->where('district_id', $request['kecamatan'])
            ->where('survey_location_id', $request['survey_location'])
            ->whereDate('survey_date', $comparison, $dateNow)
            ->get();

        return $commodityPrice;
    }

    /**
     * @param $commodityPriceSearches
     * @param $previousCommodityPriceSearches
     * @param $previousIsNotExist
     * @return array
     */
    private function setCommodityPriceResult($commodityPriceSearches,
                                             $previousCommodityPriceSearches,
                                             $previousIsNotExist)
    {
        $result = [];

        if (!empty($commodityPriceSearches) && !empty($previousCommodityPriceSearches)) {
            foreach ($commodityPriceSearches as $commodityPriceSearch) {
                $collectedPreviousCommodityPricesSearch = collect($previousCommodityPriceSearches)
                    ->groupBy('commodity_id');
                    if (isset($collectedPreviousCommodityPricesSearch[$commodityPriceSearch->commodity_id])) {
                        $finalCommodityPricesCollection =
                            collect($collectedPreviousCommodityPricesSearch[$commodityPriceSearch->commodity_id])
                                ->sortByDesc('survey_date')
                                ->first();

                        $previousIsNotExist = false;

                        if (!isset($result[$commodityPriceSearch->commodity_id]['price_now'])) {
                            $result[$commodityPriceSearch->commodity_id]['price_now'] = 0;
                        }

                        if (!isset($result[$commodityPriceSearch->commodity_id]['previous_price'])) {
                            $result[$commodityPriceSearch->commodity_id]['previous_price'] = 0;
                        }

                        $result[$commodityPriceSearch->commodity_id]['price_now'] += ($commodityPriceSearch->price_a + $commodityPriceSearch->price_b) / 2;
                        $result[$commodityPriceSearch->commodity_id]['previous_price'] += ($finalCommodityPricesCollection->price_a + $finalCommodityPricesCollection->price_b) / 2;
                    }

                if ($previousIsNotExist) {
                    if (!isset($result[$commodityPriceSearch->commodity_id]['price_now']) &&
                        !isset($result[$commodityPriceSearch->commodity_id]['previous_price'])
                    ) {
                        $result[$commodityPriceSearch->commodity_id]['price_now'] = 0;
                        $result[$commodityPriceSearch->commodity_id]['previous_price'] = 0;
                    }

                    $result[$commodityPriceSearch->commodity_id]['price_now'] += ($commodityPriceSearch->price_a + $commodityPriceSearch->price_b) / 2;
                    $result[$commodityPriceSearch->commodity_id]['previous_price'] = 0;
                }
            }
        }

        return $result;
    }
}