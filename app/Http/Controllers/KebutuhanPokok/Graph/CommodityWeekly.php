<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 7:55
 */

namespace App\Http\Controllers\KebutuhanPokok\Graph;

use Illuminate\Http\Request;

trait CommodityWeekly
{

    /**
     * @var array
     */
    private $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCommodityWeekly(Request $request)
    {

        $survey_location_id = $this->survey_location[0]->id;

        $month_id = $this->months[0]['id'];

        if ($commodityPriceRequest = $request->input()) {
            $survey_location_id = $commodityPriceRequest['survey_location'];
            $month_id = $commodityPriceRequest['month'];
        }

        $commodityPriceSelected = [];

        $commodityPrices = $this->commodityPrice->where('survey_location_id' , $survey_location_id)->groupBy('commodity_id');

        foreach($commodityPrices as $key => $items){
            foreach($items as $item){

                $date = date_format(date_create($item->survey_date), 'd-m-Y');

                if (date("m",strtotime($date)) == $month_id) {
                    if (!isset($commodityPriceSelected[$key][$date])) {
                        $commodityPriceSelected[$key][$date] = 0;
                    }

                    $commodityPriceSelected[$key][$date] += ($item->price_a + $item->price_b) / 2;
                }
            }
        }

        if ($month_id < 10) {
            $month_id = '0'.$month_id;
        }

        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Tampil Harga Komoditas Mingguan',
            'surveyLocations' => $this->survey_location,
            'surveyLocationSelected' => $survey_location_id,
            'months' => $this->months,
            'monthSelected' => $month_id,
            'commodities' => $this->commodity,
            'commodityPriceSelected' => $commodityPriceSelected,
        ];

        return view('kebutuhan-pokok.graph.commodity-weekly', $data);
    }
}