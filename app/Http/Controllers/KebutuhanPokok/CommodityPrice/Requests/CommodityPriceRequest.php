<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 15:34
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityPrice\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CommodityPriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commodity' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'price_a' => 'required|numeric',
            'price_b' => 'required|numeric',
            'survey_location' => 'required',
            'survey_date' => 'required',
            'commodityPrice.*' => 'required'
        ];
    }
}