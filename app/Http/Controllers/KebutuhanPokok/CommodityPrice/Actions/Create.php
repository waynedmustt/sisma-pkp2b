<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 14:43
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityPrice\Actions;


use App\Http\Controllers\KebutuhanPokok\CommodityPrice\Requests\CommodityPriceRequest;
use App\Http\Models\CommodityPrice;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Harga Komoditas',
            'provinces' => $this->province,
            'commodities' => $this->commodity,
            'cities' => $this->city,
            'districts' => $this->district,
            'survey_locations' => $this->survey_location
        ];

        return view('kebutuhan-pokok.commodity-price.action.create', $data);
    }

    /**
     * @param CommodityPriceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(CommodityPriceRequest $request)
    {
        $commodityPriceInput = $request->except(['_token', 'submit']);

        if (isset($commodityPriceInput['commodityPrice'])) {
            foreach ($commodityPriceInput['commodityPrice'] as $commodityPriceItem) {

                $commodityPrice = new CommodityPrice;

                $commodityPrice->commodity_id = $commodityPriceItem['commodity'];
                $commodityPrice->province_id = $commodityPriceItem['province'];
                $commodityPrice->city_id = $commodityPriceItem['city'];
                $commodityPrice->district_id = $commodityPriceItem['district'];
                $commodityPrice->price_a = $commodityPriceItem['priceA'];
                $commodityPrice->price_b = $commodityPriceItem['priceB'];
                $commodityPrice->survey_location_id = $commodityPriceItem['surveyLocation'];
                $commodityPrice->survey_date = $commodityPriceItem['surveyDate'];

                $commodityPrice->save();
            }

            return redirect('kebutuhan-pokok/commodity-price')->with('success' , 'Data Recorded!');
        }

        return redirect('kebutuhan-pokok/commodity-price')->with('failed' , 'Commodity Price did not input correctly!');
    }
}