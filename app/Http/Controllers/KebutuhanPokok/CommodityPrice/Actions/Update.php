<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 15:44
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityPrice\Actions;


use App\Http\Controllers\KebutuhanPokok\CommodityPrice\Requests\CommodityPriceRequest;
use App\Http\Models\CommodityPrice;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($commodityPrice = $this->commodityPrice->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Harga Komoditas',
                    'commodityPrice' => $commodityPrice,
                    'provinces' => $this->province,
                    'commodities' => $this->commodity,
                    'cities' => $this->city,
                    'districts' => $this->district,
                    'survey_locations' => $this->survey_location
                ];

                return view('kebutuhan-pokok.commodity-price.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param CommodityPriceRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CommodityPriceRequest $request, $id)
    {
        $commodityPriceInput = $request->except(['_token', 'submit', '_method']);

        $commodityPrice = CommodityPrice::find($id);

        $commodityPrice->commodity_id = $commodityPriceInput['commodity'];
        $commodityPrice->province_id = $commodityPriceInput['provinsi'];
        $commodityPrice->city_id = $commodityPriceInput['kota'];
        $commodityPrice->district_id = $commodityPriceInput['kecamatan'];
        $commodityPrice->price_a = $commodityPriceInput['price_a'];
        $commodityPrice->price_b = $commodityPriceInput['price_b'];
        $commodityPrice->survey_location_id = $commodityPriceInput['survey_location'];
        $commodityPrice->survey_date = $commodityPriceInput['survey_date'];

        $commodityPrice->save();

        return redirect('kebutuhan-pokok/commodity-price')->with('success' , 'Data Updated!');
    }
}