<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 15:42
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityPrice\Actions;


use App\Http\Models\CommodityPrice;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($user = CommodityPrice::find($decrypted)) {

                $user->delete();

                return redirect('kebutuhan-pokok/commodity-price')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}