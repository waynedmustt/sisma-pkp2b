<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 12:58
 */

namespace App\Http\Controllers\KebutuhanPokok\CommodityPrice;


use App\Http\Controllers\Controller;
use App\Http\Models\City;
use App\Http\Models\Commodity;
use App\Http\Models\CommodityPrice;
use App\Http\Models\District;
use App\Http\Models\Province;
use App\Http\Models\SurveyLocation;
use App\Http\Models\Year;
use Illuminate\Http\Request;

class CommodityPriceController extends Controller
{
    /**
     * @var array
     */
    private $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodityPrice, $commodity, $province, $city, $district, $survey_location;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * CommodityPriceController constructor.
     */
    public function __construct()
    {
        $this->commodityPrice = CommodityPrice::all();
        $this->commodity = Commodity::all();
        $this->province = Province::all();
        $this->city = City::all();
        $this->district = District::all();
        $this->survey_location = SurveyLocation::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        if ($year = $this->year->first()) {

            $monthSelected = $this->months[0]['id'];
            $yearSelected = date('Y');
            $commodityPrices = $this->commodityPrice;

            if ($commodityPriceRequest = $request->input()) {
                $monthSelected = $commodityPriceRequest['month'];
                $yearSelected = $commodityPriceRequest['year'];
                $commodityPrices = CommodityPrice::whereMonth('survey_date', '=', $monthSelected)
                    ->whereYear('survey_date', '=', $yearSelected)
                    ->get();
            }

            $data = [
                'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Harga Komoditas',
                'provinces' => $this->province,
                'commodities' => $this->commodity,
                'survey_locations' => $this->survey_location,
                'commodityPrices' => $commodityPrices,
                'yearSelected' => $yearSelected,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
                'months' => $this->months,
                'monthSelected' => $monthSelected
            ];

            return view('kebutuhan-pokok.commodity-price.index', $data);
        }

        return redirect('kebutuhan-pokok')->with('failed' , 'Year has not been input!');
    }
}