<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:25
 */

namespace App\Http\Controllers\KebutuhanPokok\Commodity;


use App\Http\Controllers\Controller;
use App\Http\Models\Commodity;

class CommodityController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodity;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * CommodityController constructor.
     */
    public function __construct()
    {
        $this->commodity = Commodity::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Komoditas',
            'commodities' => $this->commodity,
        ];

        return view('kebutuhan-pokok.commodity.index', $data);
    }
}