<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:36
 */

namespace App\Http\Controllers\KebutuhanPokok\Commodity\Actions;


use App\Http\Controllers\KebutuhanPokok\Commodity\Requests\CommodityRequest;
use App\Http\Models\Commodity;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Komoditas Baru'
        ];

        return view('kebutuhan-pokok.commodity.action.create', $data);
    }

    /**
     * @param CommodityRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(CommodityRequest $request)
    {
        $commodityInput = $request->except(['_token', 'submit']);

        $path = '';

        if ($request->hasFile('gambar'))
        {
            $request->file('gambar')->storeAs('commodities', $request->file('gambar')->getClientOriginalName(),
                'public');

            $path = '/commodities/' . $request->file('gambar')->getClientOriginalName();
        }

        $commodity = new Commodity;

        $commodity->name = $commodityInput['name'];
        $commodity->unit = $commodityInput['unit'];
        $commodity->gambar = $path;

        $commodity->save();

        return redirect('kebutuhan-pokok/commodity')->with('success' , 'Data Recorded!');
    }
}