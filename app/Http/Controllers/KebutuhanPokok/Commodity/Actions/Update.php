<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 12:48
 */

namespace App\Http\Controllers\KebutuhanPokok\Commodity\Actions;


use App\Http\Controllers\KebutuhanPokok\Commodity\Requests\CommodityRequest;
use App\Http\Models\Commodity;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($commodity = $this->commodity->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Komoditas ' . $commodity->name,
                    'commodity' => $commodity,
                ];

                return view('kebutuhan-pokok.commodity.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param CommodityRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CommodityRequest $request, $id)
    {
        $commodityInput = $request->except(['_token', 'submit', '_method']);

        $path = $commodityInput['previous_picture'];

        if ($request->hasFile('gambar'))
        {
            $request->file('gambar')->storeAs('commodities', $request->file('gambar')->getClientOriginalName(),
                'public');

            $path = '/commodities/' . $request->file('gambar')->getClientOriginalName();
        }

        $commodity = Commodity::find($id);

        $commodity->name = $commodityInput['name'];
        $commodity->unit = $commodityInput['unit'];
        $commodity->gambar = $path;

        $commodity->save();

        return redirect('kebutuhan-pokok/commodity')->with('success' , 'Data Updated!');
    }
}