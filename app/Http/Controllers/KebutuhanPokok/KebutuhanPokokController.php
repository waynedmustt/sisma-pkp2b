<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 6:20
 */

namespace App\Http\Controllers\KebutuhanPokok;


use App\Http\Controllers\Controller;
use App\Http\Models\Commodity;

class KebutuhanPokokController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodity;

    /**
     * KebutuhanPokokController constructor.
     */
    public function __construct()
    {
        $this->commodity = Commodity::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Beranda',
            'commodities' => $this->commodity
        ];

        return view('kebutuhan-pokok.welcome', $data);
    }
}