<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 15:59
 */

namespace App\Http\Controllers\KebutuhanPokok\Province;


use App\Http\Controllers\Controller;
use App\Http\Controllers\KebutuhanPokok\Province\Actions\Action;
use App\Http\Models\Province;

class ProvinceController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $province;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * ProvinceController constructor.
     */
    public function __construct()
    {
        $this->province = Province::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Provinsi',
            'provinces' => $this->province
        ];

        return view('kebutuhan-pokok.province.index', $data);
    }
}