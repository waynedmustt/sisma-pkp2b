<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:19
 */

namespace App\Http\Controllers\KebutuhanPokok\Province\Actions;


use App\Http\Controllers\KebutuhanPokok\Province\Requests\ProvinceRequest;
use App\Http\Models\Province;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($province = $this->province->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Provinsi ' . $province->name,
                    'province' => $province,
                ];

                return view('kebutuhan-pokok.province.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param ProvinceRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(ProvinceRequest $request, $id)
    {
        $province_input = $request->except(['_token', 'submit', '_method']);

        $province = Province::find($id);

        $province->name = $province_input['name'];

        $province->save();

        return redirect('kebutuhan-pokok/province')->with('success' , 'Data Updated!');
    }
}