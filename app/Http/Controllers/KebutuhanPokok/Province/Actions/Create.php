<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:09
 */

namespace App\Http\Controllers\KebutuhanPokok\Province\Actions;


use App\Http\Controllers\KebutuhanPokok\Province\Requests\ProvinceRequest;
use App\Http\Models\Province;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Tambah Data Provinsi',
        ];

        return view('kebutuhan-pokok.province.action.create', $data);
    }

    /**
     * @param ProvinceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(ProvinceRequest $request)
    {
        $province_input = $request->except(['_token', 'submit']);

        $province = new Province;

        $province->name = $province_input['name'];

        $province->save();

        return redirect('kebutuhan-pokok/province')->with('success' , 'Data Recorded!');
    }
}