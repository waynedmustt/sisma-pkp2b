<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 17:41
 */

namespace App\Http\Controllers\KebutuhanPokok\SurveyLocation;


use App\Http\Controllers\Controller;
use App\Http\Models\District;
use App\Http\Models\SurveyLocation;

class SurveyLocationController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $survey_location, $district;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * SurveyLocationController constructor.
     */
    public function __construct()
    {
        $this->district = District::all();
        $this->survey_location = SurveyLocation::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Lokasi Pemantauan',
            'surveyLocations' => $this->survey_location,
            'districts' => $this->district
        ];

        return view('kebutuhan-pokok.survey-location.index', $data);
    }
}