<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 17:49
 */

namespace App\Http\Controllers\KebutuhanPokok\SurveyLocation\Actions;


use App\Http\Controllers\KebutuhanPokok\SurveyLocation\Requests\SurveyLocationRequest;
use App\Http\Models\SurveyLocation;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Tambah Data Lokasi Pemantauan',
            'districts' => $this->district
        ];

        return view('kebutuhan-pokok.survey-location.action.create', $data);
    }

    /**
     * @param SurveyLocationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(SurveyLocationRequest $request)
    {
        $surveyLocation_input = $request->except(['_token', 'submit']);

        $surveyLocation = new SurveyLocation;

        $surveyLocation->name = $surveyLocation_input['name'];
        $surveyLocation->district_id = $surveyLocation_input['district'];
        $surveyLocation->description = $surveyLocation_input['description'];

        $surveyLocation->save();

        return redirect('kebutuhan-pokok/survey-location')->with('success' , 'Data Recorded!');
    }
}