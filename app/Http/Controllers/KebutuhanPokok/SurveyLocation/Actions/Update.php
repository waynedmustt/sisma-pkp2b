<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 17:59
 */

namespace App\Http\Controllers\KebutuhanPokok\SurveyLocation\Actions;


use App\Http\Controllers\KebutuhanPokok\SurveyLocation\Requests\SurveyLocationRequest;
use App\Http\Models\SurveyLocation;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($surveyLocation = $this->survey_location->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Lokasi Pemantauan ' . $surveyLocation->name,
                    'surveyLocation' => $surveyLocation,
                    'districts' => $this->district
                ];

                return view('kebutuhan-pokok.survey-location.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param SurveyLocationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(SurveyLocationRequest $request, $id)
    {
        $surveyLocation_input = $request->except(['_token', 'submit', '_method']);

        $surveyLocation = SurveyLocation::find($id);

        $surveyLocation->name = $surveyLocation_input['name'];
        $surveyLocation->district_id = $surveyLocation_input['district'];
        $surveyLocation->description = $surveyLocation_input['description'];

        $surveyLocation->save();

        return redirect('kebutuhan-pokok/survey-location')->with('success' , 'Data Updated!');
    }
}