<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 17:56
 */

namespace App\Http\Controllers\KebutuhanPokok\SurveyLocation\Actions;


use App\Http\Models\SurveyLocation;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($surveyLocation = SurveyLocation::find($decrypted)) {

                $surveyLocation->delete();

                return redirect('kebutuhan-pokok/survey-location')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}