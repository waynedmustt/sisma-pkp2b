<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/11/16
 * Time: 5:45
 */

namespace App\Http\Controllers\KebutuhanPokok\PriceCoefficient;


use Illuminate\Support\Facades\App;

trait Printer
{
    /**
     * @param $surveyLocationId
     * @param $yearSelected
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getPrint($surveyLocationId, $yearSelected)
    {

        if ($year = $this->year->first()) {
            $result = [];
            $resultDate = [];
            $counter = 0;

            $commodityPrices = $this->commodity_price
                ->where('survey_location_id', $surveyLocationId);

            foreach ($commodityPrices as $commodityPrice) {
                if (date('Y', strtotime($commodityPrice->survey_date)) == $yearSelected) {
                    $date = date_format(date_create($commodityPrice->survey_date), 'd-m-Y');

                    if (!isset($result[$commodityPrice->commodity_id][$date])) {
                        $result[$commodityPrice->commodity_id][$date] = 0;
                    }

                    $result[$commodityPrice->commodity_id][$date] += ($commodityPrice->price_a + $commodityPrice->price_b) / 2;

                    if (!isset($resultDate[$date])) {
                        $resultDate[$date] = $counter;
                        $counter += 1;
                    }
                }
            }

            foreach ($commodityPrices as $commodityPrice) {

                if (isset($result[$commodityPrice->commodity_id])) {
                    $resetResult = collect($result[$commodityPrice->commodity_id])->values()->all();
                    if (!isset($result[$commodityPrice->commodity_id]['max'])) {
                        $result[$commodityPrice->commodity_id]['max'] = collect($resetResult)->max();
                    }

                    if (!isset($result[$commodityPrice->commodity_id]['min'])) {
                        $result[$commodityPrice->commodity_id]['min'] = collect($resetResult)->min();
                    }
                }
            }

            $data = [
                'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Cetak Koefisien Harga',
                'commodityPrices' => $this->commodity_price,
                'commodities' => $this->commodity,
                'surveyLocations' => $this->survey_location,
                'results' => $result,
                'resultDatesFlipped' => collect($resultDate)->flip(),
                'counter' => $counter,
                'months' => $this->months,
                'surveyLocationId' => $surveyLocationId,
                'yearSelected' => $yearSelected,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
            ];

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('kebutuhan-pokok.price-coefficient.print', $data)->setPaper('a3', 'landscape');
            return $pdf->stream();
        }

        return redirect('kebutuhan-pokok')->with('failed' , 'Year has not been input!');
    }

}