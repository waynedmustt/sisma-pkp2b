<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 14/11/16
 * Time: 21:04
 */

namespace App\Http\Controllers\KebutuhanPokok\PriceCoefficient;


use App\Http\Controllers\Controller;
use App\Http\Models\Commodity;
use App\Http\Models\CommodityPrice;
use App\Http\Models\SurveyLocation;
use App\Http\Models\Year;
use Illuminate\Http\Request;

class PriceCoefficientController extends Controller
{
    /**
     * @var array
     */
    private $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $commodity, $commodity_price, $survey_location, $year;

    use Printer;

    /**
     * PriceCoefficientController constructor.
     */
    public function __construct()
    {
        $this->commodity = Commodity::all();
        $this->commodity_price = CommodityPrice::all();
        $this->survey_location = SurveyLocation::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {

        if ($year = $this->year->first()) {
            $result = [];
            $resultDate = [];
            $counter = 0;
            $surveyLocationId = $this->survey_location[0]->id;
            $yearSelected = date('Y');

            if ($commodityPriceRequest = $request->input()) {
                $surveyLocationId = $commodityPriceRequest['surveyLocation'];
                $yearSelected = $commodityPriceRequest['year'];
            }

            $commodityPrices = $this->commodity_price
                ->where('survey_location_id', $surveyLocationId);

            foreach ($commodityPrices as $commodityPrice) {
                if (date('Y', strtotime($commodityPrice->survey_date)) == $yearSelected) {
                    $date = date_format(date_create($commodityPrice->survey_date), 'd-m-Y');

                    if (!isset($result[$commodityPrice->commodity_id][$date])) {
                        $result[$commodityPrice->commodity_id][$date] = 0;
                    }

                    $result[$commodityPrice->commodity_id][$date] += ($commodityPrice->price_a + $commodityPrice->price_b) / 2;

                    if (!isset($resultDate[$date])) {
                        $resultDate[$date] = $counter;
                        $counter += 1;
                    }
                }
            }

            foreach ($commodityPrices as $commodityPrice) {

                if (isset($result[$commodityPrice->commodity_id])) {
                    $resetResult = collect($result[$commodityPrice->commodity_id])->values()->all();
                    if (!isset($result[$commodityPrice->commodity_id]['max'])) {
                        $result[$commodityPrice->commodity_id]['max'] = collect($resetResult)->max();
                    }

                    if (!isset($result[$commodityPrice->commodity_id]['min'])) {
                        $result[$commodityPrice->commodity_id]['min'] = collect($resetResult)->min();
                    }
                }
            }

            $data = [
                'title' => 'Informasi Hasil Pemantauan Barang Pokok Bersubsidi - Koefisien Harga',
                'commodityPrices' => $this->commodity_price,
                'commodities' => $this->commodity,
                'surveyLocations' => $this->survey_location,
                'results' => $result,
                'counter' => $counter,
                'resultDatesFlipped' => collect($resultDate)->flip(),
                'months' => $this->months,
                'surveyLocationId' => $surveyLocationId,
                'yearSelected' => $yearSelected,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
            ];

            return view('kebutuhan-pokok.price-coefficient.index', $data);
        }

        return redirect('kebutuhan-pokok')->with('failed' , 'Year has not been input!');
    }
}