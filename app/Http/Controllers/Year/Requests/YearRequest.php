<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/11/16
 * Time: 6:05
 */

namespace App\Http\Controllers\Year\Requests;


use Illuminate\Foundation\Http\FormRequest;

class YearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_year' => 'required',
            'end_year' => 'required',
        ];
    }
}