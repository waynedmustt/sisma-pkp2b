<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/11/16
 * Time: 4:25
 */

namespace App\Http\Controllers\Year;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Year\Requests\YearRequest;
use App\Http\Models\Year;

class YearController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $year;

    /**
     * YearController constructor.
     */
    public function __construct()
    {
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {

        $data = [
            'title' => 'Tahun',
            'year' => count($this->year) == 0 ? $this->year : $this->year->first()
        ];

        return view('year.index', $data);
    }

    /**
     * @param YearRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex(YearRequest $request, $id=false)
    {
        $years_input = $request->except(['_token', 'submit']);

        $roles = new Year;

        if ($id) {
            $roles = Year::find($id);
        }

        $roles->start_year = $years_input['start_year'];
        $roles->end_year = $years_input['end_year'];

        $roles->save();

        return redirect('/')->with('success' , 'Year Recorded!');
    }
}