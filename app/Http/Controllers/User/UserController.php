<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 29/10/16
 * Time: 19:01
 */

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Http\Models\Role;
use App\Http\Models\RoleUser;
use App\Http\Models\User;

class UserController extends Controller
{
    private $user, $role;

    use Actions\Create, Actions\Delete, Actions\Update, Actions\Status;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->role = Role::all();
        $this->user = User::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $role_user = RoleUser::all();

        $data = [
            'title' => 'Sisma Apps - User',
            'users' => $this->user,
            'roles' => $this->role,
            'role_users' => $role_user
        ];

        return view('user.index', $data);
    }
}