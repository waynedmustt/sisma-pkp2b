<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 5:38
 */

namespace App\Http\Controllers\User\Actions;


use App\Http\Models\User;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete user data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id){

        try {

            $decrypted = decrypt($id);

            if ($user = User::find($decrypted)) {

                $user->delete();

                return redirect('user')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}