<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 9:39
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Models\User;

class AuthController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = User::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(LoginRequest $request)
    {
        $login_input = $request->except(['_token', 'submit']);
        if ($user = User::where('username' , $login_input['username'])->where('password' , md5($login_input['password']))->where('is_active', 1)->first()) {
            session(['userLoggedIn' => $login_input['username']]);

            return redirect('/');
        }

        return redirect('auth/login');
    }

    public function getLogout()
    {
        session()->flush();

        return redirect('auth/login');
    }
}