<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:54
 */

namespace App\Http\Controllers\District\Actions;


use App\Http\Controllers\District\Requests\DistrictRequest;
use App\Http\Models\District;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Tambah Data Kecamatan',
            'cities' => $this->city
        ];

        return view('kebutuhan-pokok.district.action.create', $data);
    }

    /**
     * @param DistrictRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(DistrictRequest $request)
    {
        $district_input = $request->except(['_token', 'submit']);

        $district = new District;

        $district->name = $district_input['name'];
        $district->city_id = $district_input['city'];

        $district->save();

        return redirect('district')->with('success' , 'Data Recorded!');
    }
}