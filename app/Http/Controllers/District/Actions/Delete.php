<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:59
 */

namespace App\Http\Controllers\District\Actions;


use App\Http\Models\District;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($district = District::find($decrypted)) {

                $district->delete();

                return redirect('district')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}