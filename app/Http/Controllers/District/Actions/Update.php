<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 17:01
 */

namespace App\Http\Controllers\District\Actions;


use App\Http\Controllers\District\Requests\DistrictRequest;
use App\Http\Models\District;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($district = $this->district->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Perbaharui Kecamatan ' . $district->name,
                    'district' => $district,
                    'cities' => $this->city
                ];

                return view('kebutuhan-pokok.district.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param DistrictRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(DistrictRequest $request, $id)
    {
        $district_input = $request->except(['_token', 'submit']);

        $district = District::find($id);

        $district->name = $district_input['name'];
        $district->city_id = $district_input['city'];

        $district->save();

        return redirect('district')->with('success' , 'Data Updated!');
    }
}