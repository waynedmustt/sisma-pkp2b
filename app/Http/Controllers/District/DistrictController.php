<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 16:47
 */

namespace App\Http\Controllers\District;


use App\Http\Controllers\Controller;
use App\Http\Models\City;
use App\Http\Models\District;

class DistrictController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $city, $district;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * DistrictController constructor.
     */
    public function __construct()
    {
        $this->city = City::all();
        $this->district = District::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Daftar Kecamatan',
            'cities' => $this->city,
            'districts' => $this->district
        ];

        return view('kebutuhan-pokok.district.index', $data);
    }
}