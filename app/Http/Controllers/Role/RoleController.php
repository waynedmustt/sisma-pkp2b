<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 6:06
 */

namespace App\Http\Controllers\Role;


use App\Http\Controllers\Controller;
use App\Http\Models\Role;

class RoleController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $role;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->role = Role::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Sisma Apps - Role',
            'roles' => $this->role
        ];

        return view('role.index', $data);
    }
}