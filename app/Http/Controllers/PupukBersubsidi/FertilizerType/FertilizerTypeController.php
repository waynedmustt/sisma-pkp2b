<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 7:56
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerType;


use App\Http\Controllers\Controller;
use App\Http\Models\FertilizerType;
use App\Http\Models\Year;

class FertilizerTypeController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $fertilizer_type, $year;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * FertilizerController constructor.
     */
    public function __construct()
    {
        $this->fertilizer_type = FertilizerType::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Jenis Pupuk',
            'fertilizerTypes' => $this->fertilizer_type
        ];

        return view('pupuk-bersubsidi.fertilizer-type.index', $data);
    }
}