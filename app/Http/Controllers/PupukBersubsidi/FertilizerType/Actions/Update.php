<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 8:10
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerType\Actions;


use App\Http\Controllers\PupukBersubsidi\FertilizerType\Requests\FertilizerTypeRequest;
use App\Http\Models\FertilizerType;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerType = $this->fertilizer_type->where('id', $decrypted)->first()) {

                if ($year = $this->year->first()) {
                    $startYear = (int) $year->start_year;
                    $endYear = (int) $year->end_year;

                    $data = [
                        'title' => 'Sisma Apps - Perbaharui Jenis Pupuk ' . $fertilizerType->name,
                        'fertilizerType' => $fertilizerType,
                        'startYear' => $startYear,
                        'endYear' => $endYear
                    ];

                    return view('pupuk-bersubsidi.fertilizer-type.action.update', $data);
                }

                return redirect('pupuk-bersubsidi/fertilizer-type')->with('failed' , 'Year has not been input!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param FertilizerTypeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(FertilizerTypeRequest $request, $id)
    {
        $fertilizerTypeInput = $request->except(['_token', 'submit']);

        $path = $fertilizerTypeInput['previous_picture'];

        if ($request->hasFile('gambar'))
        {
            $request->file('gambar')->storeAs('fertilizers', $request->file('gambar')->getClientOriginalName(),
                'public');

            $path = '/fertilizers/' . $request->file('gambar')->getClientOriginalName();
        }

        $fertilizerType = FertilizerType::find($id);

        $fertilizerType->name = $fertilizerTypeInput['name'];
        $fertilizerType->year = $fertilizerTypeInput['year'];
        $fertilizerType->gambar = $path;

        $fertilizerType->save();

        return redirect('pupuk-bersubsidi/fertilizer-type')->with('success' , 'Data Updated!');
    }
}