<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 8:02
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerType\Actions;


use App\Http\Controllers\PupukBersubsidi\FertilizerType\Requests\FertilizerTypeRequest;
use App\Http\Models\FertilizerType;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int) $year->start_year;
            $endYear = (int) $year->end_year;

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Jenis Pupuk',
                'startYear' => $startYear,
                'endYear' => $endYear
            ];

            return view('pupuk-bersubsidi.fertilizer-type.action.create', $data);
        }

        return redirect('pupuk-bersubsidi/fertilizer-type')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param FertilizerTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(FertilizerTypeRequest $request)
    {
        $fertilizerTypeInput = $request->except(['_token', 'submit']);

        $path = '';

        if ($request->hasFile('gambar'))
        {
            $request->file('gambar')->storeAs('fertilizers', $request->file('gambar')->getClientOriginalName(),
                'public');

            $path = '/fertilizers/' . $request->file('gambar')->getClientOriginalName();
        }

        $fertilizerType = new FertilizerType;

        $fertilizerType->name = $fertilizerTypeInput['name'];
        $fertilizerType->year = $fertilizerTypeInput['year'];
        $fertilizerType->gambar = $path;

        $fertilizerType->save();

        return redirect('pupuk-bersubsidi/fertilizer-type')->with('success' , 'Data Recorded!');
    }
}