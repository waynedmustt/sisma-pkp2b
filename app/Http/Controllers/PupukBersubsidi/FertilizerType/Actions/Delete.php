<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 8:09
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerType\Actions;


use App\Http\Models\FertilizerType;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerType = FertilizerType::find($decrypted)) {

                $fertilizerType->delete();

                return redirect('pupuk-bersubsidi/fertilizer-type')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}