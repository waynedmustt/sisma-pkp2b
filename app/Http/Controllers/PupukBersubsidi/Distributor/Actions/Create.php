<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:40
 */

namespace App\Http\Controllers\PupukBersubsidi\Distributor\Actions;


use App\Http\Controllers\PupukBersubsidi\Distributor\Requests\DistributorRequest;
use App\Http\Models\Distributor;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int) $year->start_year;
            $endYear = (int) $year->end_year;

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Distributor',
                'startYear' => $startYear,
                'endYear' => $endYear
            ];

            return view('pupuk-bersubsidi.distributor.action.create', $data);

        }

        return redirect('pupuk-bersubsidi/distributor')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param DistributorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(DistributorRequest $request)
    {
        $distributor_input = $request->except(['_token', 'submit']);

        $distributor = new Distributor;

        $distributor->name = $distributor_input['name'];
        $distributor->address = $distributor_input['address'];
        $distributor->year = $distributor_input['year'];
        $distributor->director_name = $distributor_input['director_name'];
        $distributor->produsen_name = $distributor_input['produsen_name'];
        $distributor->handphone_number = $distributor_input['handphone_number'];

        $distributor->save();

        return redirect('pupuk-bersubsidi/distributor')->with('success' , 'Data Recorded!');
    }
}