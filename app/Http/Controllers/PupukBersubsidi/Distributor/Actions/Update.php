<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:46
 */

namespace App\Http\Controllers\PupukBersubsidi\Distributor\Actions;


use App\Http\Controllers\PupukBersubsidi\Distributor\Requests\DistributorRequest;
use App\Http\Models\Distributor;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($distributor = $this->distributor->where('id', $decrypted)->first()) {

                if ($year = $this->year->first()) {
                    $startYear = (int) $year->start_year;
                    $endYear = (int) $year->end_year;

                    $data = [
                        'title' => 'Sisma Apps - Perbaharui Distributor ' . $distributor->name,
                        'distributor' => $distributor,
                        'startYear' => $startYear,
                        'endYear' => $endYear
                    ];

                    return view('pupuk-bersubsidi.distributor.action.update', $data);
                }

                return redirect('pupuk-bersubsidi/distributor')->with('failed' , 'Year has not been input!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param DistributorRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(DistributorRequest $request, $id)
    {
        $distributor_input = $request->except(['_token', 'submit']);

        $distributor = Distributor::find($id);

        $distributor->name = $distributor_input['name'];
        $distributor->year = $distributor_input['year'];
        $distributor->address = $distributor_input['address'];
        $distributor->director_name = $distributor_input['director_name'];
        $distributor->produsen_name = $distributor_input['produsen_name'];
        $distributor->handphone_number = $distributor_input['handphone_number'];

        $distributor->save();

        return redirect('pupuk-bersubsidi/distributor')->with('success' , 'Data Updated!');
    }
}