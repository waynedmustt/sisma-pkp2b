<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 06/11/16
 * Time: 19:09
 */

namespace App\Http\Controllers\PupukBersubsidi\Distributor;


use App\Http\Controllers\Controller;
use App\Http\Models\Distributor;
use App\Http\Models\Year;

class DistributorController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $distributor, $year;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * DistributorController constructor.
     */
    public function __construct()
    {
        $this->distributor = Distributor::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Distributor',
            'distributors' => $this->distributor
        ];

        return view('pupuk-bersubsidi.distributor.index', $data);
    }
}