<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:42
 */

namespace App\Http\Controllers\PupukBersubsidi\Distributor\Requests;


use Illuminate\Foundation\Http\FormRequest;

class DistributorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'year' => 'required',
            'address' => 'required',
            'director_name' => 'required',
            'produsen_name' => 'required',
            'handphone_number' => 'required|numeric',
        ];
    }
}