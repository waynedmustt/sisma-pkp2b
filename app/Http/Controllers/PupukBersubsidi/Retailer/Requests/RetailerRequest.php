<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:59
 */

namespace App\Http\Controllers\PupukBersubsidi\Retailer\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RetailerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'distributor' => 'required',
            'year' => 'required',
            'owner_name' => 'required',
            'address' => 'required',
            'handphone_number' => 'required|numeric',
            'district' => 'required',
        ];
    }
}