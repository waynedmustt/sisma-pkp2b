<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:51
 */

namespace App\Http\Controllers\PupukBersubsidi\Retailer;


use App\Http\Controllers\Controller;
use App\Http\Models\Distributor;
use App\Http\Models\District;
use App\Http\Models\Retailer;
use App\Http\Models\Year;

class RetailerController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $retailer, $distributor, $district, $year;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * RetailerController constructor.
     */
    public function __construct()
    {
        $this->retailer = Retailer::all();
        $this->distributor = Distributor::all();
        $this->district = District::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Retailer',
            'retailers' => $this->retailer,
            'distributors' => $this->distributor,
            'districts' => $this->district
        ];

        return view('pupuk-bersubsidi.retailer.index', $data);
    }
}