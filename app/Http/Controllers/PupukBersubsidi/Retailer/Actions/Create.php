<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 2:58
 */

namespace App\Http\Controllers\PupukBersubsidi\Retailer\Actions;


use App\Http\Controllers\PupukBersubsidi\Retailer\Requests\RetailerRequest;
use App\Http\Models\Retailer;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int) $year->start_year;
            $endYear = (int) $year->end_year;

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Tambah Data Retailer',
                'distributors' => $this->distributor,
                'districts' => $this->district,
                'years' => $this->year,
                'startYear' => $startYear,
                'endYear' => $endYear
            ];

            return view('pupuk-bersubsidi.retailer.action.create', $data);

        }

        return redirect('pupuk-bersubsidi/retailer')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param RetailerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(RetailerRequest $request)
    {
        $retailer_input = $request->except(['_token', 'submit']);

        $retailer = new Retailer;

        $retailer->name = $retailer_input['name'];
        $retailer->distributor_id = $retailer_input['distributor'];
        $retailer->year = $retailer_input['year'];
        $retailer->owner_name = $retailer_input['owner_name'];
        $retailer->address = $retailer_input['address'];
        $retailer->handphone_number = $retailer_input['handphone_number'];
        $retailer->district_id = $retailer_input['district'];

        $retailer->save();

        return redirect('pupuk-bersubsidi/retailer')->with('success' , 'Data Recorded!');
    }
}