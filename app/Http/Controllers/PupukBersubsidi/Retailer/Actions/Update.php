<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 3:04
 */

namespace App\Http\Controllers\PupukBersubsidi\Retailer\Actions;


use App\Http\Controllers\PupukBersubsidi\Retailer\Requests\RetailerRequest;
use App\Http\Models\Retailer;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($retailer = $this->retailer->where('id', $decrypted)->first()) {

                if ($year = $this->year->first()) {
                    $startYear = (int) $year->start_year;
                    $endYear = (int) $year->end_year;

                    $data = [
                        'title' => 'Sisma Apps - Perbaharui Pengecer ' . $retailer->name,
                        'retailer' => $retailer,
                        'distributors' => $this->distributor,
                        'years' => $this->year,
                        'startYear' => $startYear,
                        'endYear' => $endYear,
                        'districts' => $this->district
                    ];

                    return view('pupuk-bersubsidi.retailer.action.update', $data);
                }

                return redirect('pupuk-bersubsidi/retailer')->with('failed' , 'Year has not been input!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param RetailerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RetailerRequest $request, $id)
    {
        $retailer_input = $request->except(['_token', 'submit']);

        $retailer = Retailer::find($id);

        $retailer->name = $retailer_input['name'];
        $retailer->distributor_id = $retailer_input['distributor'];
        $retailer->year = $retailer_input['year'];
        $retailer->owner_name = $retailer_input['owner_name'];
        $retailer->address = $retailer_input['address'];
        $retailer->handphone_number = $retailer_input['handphone_number'];
        $retailer->district_id = $retailer_input['district'];

        $retailer->save();

        return redirect('pupuk-bersubsidi/retailer')->with('success' , 'Data Updated!');
    }
}