<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 3:02
 */

namespace App\Http\Controllers\PupukBersubsidi\Retailer\Actions;


use App\Http\Models\Retailer;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($retailer = Retailer::find($decrypted)) {

                $retailer->delete();

                return redirect('pupuk-bersubsidi/retailer')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}