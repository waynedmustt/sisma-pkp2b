<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 7:45
 */

namespace App\Http\Controllers\PupukBersubsidi\Village\Actions;


use App\Http\Controllers\PupukBersubsidi\Village\Requests\VillageRequest;
use App\Http\Models\Village;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($village = $this->village->where('id', $decrypted)->first()) {

                $data = [
                    'title' => 'Sisma Apps - Perbaharui Desa ' . $village->name,
                    'village' => $village,
                    'districts' => $this->district
                ];

                return view('pupuk-bersubsidi.village.action.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param VillageRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(VillageRequest $request, $id)
    {
        $village_input = $request->except(['_token', 'submit']);

        $village = Village::find($id);

        $village->name = $village_input['name'];
        $village->district_id = $village_input['district'];

        $village->save();

        return redirect('pupuk-bersubsidi/village')->with('success' , 'Data Updated!');
    }
}