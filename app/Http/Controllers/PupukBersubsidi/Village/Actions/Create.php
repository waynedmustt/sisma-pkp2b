<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 7:37
 */

namespace App\Http\Controllers\PupukBersubsidi\Village\Actions;


use App\Http\Controllers\PupukBersubsidi\Village\Requests\VillageRequest;
use App\Http\Models\Village;

trait Create
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Tambah Data Desa',
            'districts' => $this->district
        ];

        return view('pupuk-bersubsidi.village.action.create', $data);
    }

    /**
     * @param VillageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(VillageRequest $request)
    {
        $village_input = $request->except(['_token', 'submit']);

        $village = new Village;

        $village->name = $village_input['name'];
        $village->district_id = $village_input['district'];

        $village->save();

        return redirect('pupuk-bersubsidi/village')->with('success' , 'Data Recorded!');
    }
}