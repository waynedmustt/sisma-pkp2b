<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 7:31
 */

namespace App\Http\Controllers\PupukBersubsidi\Village;


use App\Http\Controllers\Controller;
use App\Http\Models\District;
use App\Http\Models\Village;

class VillageController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $village, $district;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * VillageController constructor.
     */
    public function __construct()
    {
        $this->village = Village::all();
        $this->district = District::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Desa',
            'villages' => $this->village,
            'districts' => $this->district
        ];

        return view('pupuk-bersubsidi.village.index', $data);
    }
}