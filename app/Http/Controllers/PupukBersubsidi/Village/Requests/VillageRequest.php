<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/11/16
 * Time: 7:41
 */

namespace App\Http\Controllers\PupukBersubsidi\Village\Requests;


use Illuminate\Foundation\Http\FormRequest;

class VillageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'district' => 'required'
        ];
    }
}