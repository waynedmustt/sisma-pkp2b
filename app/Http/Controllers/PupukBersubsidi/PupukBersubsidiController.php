<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 6:21
 */

namespace App\Http\Controllers\PupukBersubsidi;


use App\Http\Controllers\Controller;
use App\Http\Models\FertilizerType;

class PupukBersubsidiController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $fertilizerType;

    /**
     * PupukBersubsidiController constructor.
     */
    public function __construct()
    {
        $this->fertilizerType = FertilizerType::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Beranda',
            'fertilizerTypes' => $this->fertilizerType
        ];
        return view('pupuk-bersubsidi.welcome', $data);
    }

}