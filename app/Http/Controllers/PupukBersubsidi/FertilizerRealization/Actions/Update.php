<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:44
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerRealization\Actions;

use App\Events\FertilizerRealizationWasCreated;
use App\Http\Controllers\PupukBersubsidi\FertilizerRealization\Requests\FertilizerRealizationRequest;
use App\Http\Models\FertilizerRealization;
use App\Http\Models\FertilizerRealizationStock;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerRealization = $this->fertilizer_realization->where('id', $decrypted)->first()) {

                if ($year = $this->year->first()) {
                    $startYear = (int) $year->start_year;
                    $endYear = (int) $year->end_year;

                    $data = [
                        'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Kuota Pupuk Bersubsidi',
                        'retailers' => $this->retailer,
                        'distributors' => $this->distributor,
                        'fertilizerRealization' => $fertilizerRealization,
                        'fertilizerTypes' => $this->fertilizer_type,
                        'fertilizerRealizationStocks' => $this->fertilizer_realization_stock,
                        'months' => $this->months,
                        'startYear' => $startYear,
                        'endYear' => $endYear,
                    ];

                    return view('pupuk-bersubsidi.fertilizer-realization.action.update', $data);
                }

                return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param FertilizerRealizationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(FertilizerRealizationRequest $request, $id)
    {
        $fertilizerRealizationInput = $request->except(['_token', 'submit']);

        FertilizerRealizationStock::where('fertilizer_realization_id', $id)->delete();

        $fertilizerRealization = FertilizerRealization::find($id);

        $fertilizerRealization->distributor_id = $fertilizerRealizationInput['distributor'];
        $fertilizerRealization->retailer_id = $fertilizerRealizationInput['retailer'];
        $fertilizerRealization->year = $fertilizerRealizationInput['year'];
        $fertilizerRealization->month = $this->getMonthName($fertilizerRealizationInput['month']);

        $fertilizerRealization->save();

        $index = 1;
        foreach($fertilizerRealizationInput['stock'] as $stock) {
            event(new FertilizerRealizationWasCreated($stock,
                $fertilizerRealization->id, $fertilizerRealizationInput['fertilizerType'][$index]));
            $index++;
        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('success' , 'Data Updated!');
    }
}