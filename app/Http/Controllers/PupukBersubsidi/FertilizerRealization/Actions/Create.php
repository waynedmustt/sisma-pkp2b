<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:28
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerRealization\Actions;

use App\Events\FertilizerRealizationWasCreated;
use App\Http\Controllers\PupukBersubsidi\FertilizerRealization\Requests\FertilizerRealizationRequest;
use App\Http\Models\FertilizerRealization;

trait Create
{
    private $isFillable = false;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int) $year->start_year;
            $endYear = (int) $year->end_year;

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Realisasi Pupuk Bersubsidi',
                'retailers' => $this->retailer,
                'distributors' => $this->distributor,
                'fertilizerTypes' => $this->fertilizer_type,
                'months' => $this->months,
                'startYear' => $startYear,
                'endYear' => $endYear
            ];

            return view('pupuk-bersubsidi.fertilizer-realization.action.create', $data);

        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param FertilizerRealizationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(FertilizerRealizationRequest $request)
    {
        $fertilizerRealizationInput = $request->except(['_token', 'submit']);

        foreach($fertilizerRealizationInput['fertilizerRealization'] as $fertilizerRealizationItem) {
            $fertilizerRealization = new FertilizerRealization;

            $fertilizerRealization->distributor_id = $fertilizerRealizationItem['distributor'];
            $fertilizerRealization->retailer_id = $fertilizerRealizationItem['retailer'];
            $fertilizerRealization->year = $fertilizerRealizationItem['year'];
            $fertilizerRealization->month = $fertilizerRealizationItem['month'];

            $fertilizerRealization->save();

            $index = 0;
            foreach($fertilizerRealizationItem['stock'] as $stock) {
                event(new FertilizerRealizationWasCreated($stock,
                    $fertilizerRealization->id, $fertilizerRealizationItem['fertilizerType'][$index]));
                $index++;
            }
        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('success' , 'Data Recorded!');
    }

}