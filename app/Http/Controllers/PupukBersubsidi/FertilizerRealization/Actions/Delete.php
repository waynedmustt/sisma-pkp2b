<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:43
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerRealization\Actions;

use App\Http\Models\FertilizerRealization;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerRealization = FertilizerRealization::find($decrypted)) {

                $fertilizerRealization->delete();

                return redirect('pupuk-bersubsidi/fertilizer-realization')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}