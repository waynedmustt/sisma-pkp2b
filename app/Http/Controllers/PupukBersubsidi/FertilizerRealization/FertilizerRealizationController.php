<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 6:45
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerRealization;


use App\Http\Controllers\Controller;
use App\Http\Models\Distributor;
use App\Http\Models\District;
use App\Http\Models\Fertilizer;
use App\Http\Models\FertilizerRealization;
use App\Http\Models\FertilizerRealizationStock;
use App\Http\Models\FertilizerType;
use App\Http\Models\Retailer;
use App\Http\Models\Village;
use App\Http\Models\Year;

class FertilizerRealizationController extends Controller
{
    private $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $retailer, $distributor, $fertilizer_type, $fertilizer_realization, $year, $fertilizer_realization_stock;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * FertilizerQuotaController constructor.
     */
    public function __construct()
    {
        $this->retailer = Retailer::all();
        $this->distributor = Distributor::all();
        $this->year = Year::all();
        $this->fertilizer_type = FertilizerType::all();
        $this->fertilizer_realization_stock = FertilizerRealizationStock::all();
        $this->fertilizer_realization = FertilizerRealization::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Realisasi Pupuk Bersubsidi',
            'retailers' => $this->retailer,
            'distributors' => $this->distributor,
            'fertilizerTypes' => $this->fertilizer_type,
            'fertilizerRealizationStocks' => $this->fertilizer_realization_stock,
            'fertilizerRealizations' => $this->fertilizer_realization
        ];

        return view('pupuk-bersubsidi.fertilizer-realization.index', $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getMonthName($id)
    {
        return $this->months[$id-1]['name'];
    }
}