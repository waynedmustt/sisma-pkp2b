<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 6:45
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerQuota;


use App\Http\Controllers\Controller;
use App\Http\Models\Distributor;
use App\Http\Models\District;
use App\Http\Models\Fertilizer;
use App\Http\Models\FertilizerQuota;
use App\Http\Models\FertilizerType;
use App\Http\Models\Retailer;
use App\Http\Models\Village;
use App\Http\Models\Year;

class FertilizerQuotaController extends Controller
{

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $fertilizer_quota, $fertilizer_type, $year;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * FertilizerQuotaController constructor.
     */
    public function __construct()
    {
        $this->fertilizer_quota = FertilizerQuota::all();
        $this->fertilizer_type = FertilizerType::all();
        $this->year = Year::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Kuota Pupuk Bersubsidi',
            'fertilizerQuotas' => $this->fertilizer_quota,
            'fertilizerTypes' => $this->fertilizer_type
        ];

        return view('pupuk-bersubsidi.fertilizer-quota.index', $data);
    }

}