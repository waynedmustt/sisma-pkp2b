<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:51
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FertilizerQuotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fertilizer_type' => 'required',
            'unit' => 'required',
            'year' => 'required',
            'quota' => 'required|numeric',
            'het' => 'required|numeric',
        ];
    }
}