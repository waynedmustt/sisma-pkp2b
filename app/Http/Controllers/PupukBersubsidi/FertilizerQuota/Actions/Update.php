<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:44
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Actions;

use App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Requests\FertilizerQuotaRequest;
use App\Http\Models\FertilizerQuota;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerQuota = $this->fertilizer_quota->where('id', $decrypted)->first()) {

                if ($year = $this->year->first()) {
                    $startYear = (int) $year->start_year;
                    $endYear = (int) $year->end_year;

                    $data = [
                        'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Kuota Pupuk Bersubsidi',
                        'fertilizerTypes' => $this->fertilizer_type,
                        'fertilizerQuota' => $fertilizerQuota,
                        'startYear' => $startYear,
                        'endYear' => $endYear,
                    ];

                    return view('pupuk-bersubsidi.fertilizer-quota.action.update', $data);
                }

                return redirect('pupuk-bersubsidi/fertilizer-quota')->with('failed' , 'Year has not been input!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * @param FertilizerQuotaRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(FertilizerQuotaRequest $request, $id)
    {
        $fertilizerQuotaInput = $request->except(['_token', 'submit']);

        $fertilizerQuota = FertilizerQuota::find($id);

        $fertilizerQuota->fertilizer_type_id = $fertilizerQuotaInput['fertilizer_type'];
        $fertilizerQuota->quota = (float) $fertilizerQuotaInput['quota'];
        $fertilizerQuota->het = $fertilizerQuotaInput['het'];
        $fertilizerQuota->unit = $fertilizerQuotaInput['unit'];
        $fertilizerQuota->year = $fertilizerQuotaInput['year'];

        $fertilizerQuota->save();

        return redirect('pupuk-bersubsidi/fertilizer-quota')->with('success' , 'Data Updated!');
    }
}