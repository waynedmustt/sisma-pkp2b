<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:28
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Actions;

use App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Requests\FertilizerQuotaRequest;
use App\Http\Models\FertilizerQuota;

trait Create
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        if ($year = $this->year->first()) {

            $startYear = (int) $year->start_year;
            $endYear = (int) $year->end_year;

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Kuota Pupuk Bersubsidi',
                'fertilizerTypes' => $this->fertilizer_type,
                'startYear' => $startYear,
                'endYear' => $endYear,
            ];

            return view('pupuk-bersubsidi.fertilizer-quota.action.create', $data);

        }

        return redirect('pupuk-bersubsidi/fertilizer-quota')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param FertilizerQuotaRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(FertilizerQuotaRequest $request)
    {
        $fertilizerQuotaInput = $request->except(['_token', 'submit']);

        $fertilizerQuota = new FertilizerQuota;

        $fertilizerQuota->fertilizer_type_id = $fertilizerQuotaInput['fertilizer_type'];
        $fertilizerQuota->quota = (float) $fertilizerQuotaInput['quota'];
        $fertilizerQuota->het = $fertilizerQuotaInput['het'];
        $fertilizerQuota->unit = $fertilizerQuotaInput['unit'];
        $fertilizerQuota->year = $fertilizerQuotaInput['year'];

        $fertilizerQuota->save();

        return redirect('pupuk-bersubsidi/fertilizer-quota')->with('success' , 'Data Recorded!');
    }

}