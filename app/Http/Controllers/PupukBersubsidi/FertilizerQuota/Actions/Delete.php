<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 7:43
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerQuota\Actions;

use App\Http\Models\FertilizerQuota;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * @param $id
     * @return \Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($fertilizerQuota = FertilizerQuota::find($decrypted)) {

                $fertilizerQuota->delete();

                return redirect('pupuk-bersubsidi/fertilizer-quota')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}