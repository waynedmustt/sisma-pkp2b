<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 13/11/16
 * Time: 22:31
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerReport;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

trait Distributor
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDistributor(Request $request)
    {
        if ($year = $this->year->first()) {
            $yearSelected = date('Y');
            $distributorSelected = $this->distributor[0]->id;

            if ($fertilizerDistribution = $request->input()) {
                $yearSelected = $fertilizerDistribution['year'];
                $distributorSelected = $fertilizerDistribution['distributor'];
            }

            if ($quotaTotal = $this->fertilizer_quota->where('year', $yearSelected)){
                $quotaTotal = $quotaTotal->sum('quota');
            }

            $result = $this->setDistributorTotalResult($yearSelected, $distributorSelected);

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Total Per Distributor',
                'fertilizerTypes' => $this->fertilizer_type,
                'results' => $result,
                'months' => $this->months,
                'distributors' => $this->distributor,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
                'yearSelected' => $yearSelected,
                'distributorSelected' => $distributorSelected,
                'quotaTotal' => $quotaTotal ? $quotaTotal : 0
            ];

            return view('pupuk-bersubsidi.fertilizer-report.distributor-total', $data);

        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param $distributorSelected
     * @param $yearSelected
     * @return \Illuminate\Http\RedirectResponse
     */
    public function printDistributorTotal($distributorSelected, $yearSelected)
    {
        if ($year = $this->year->first()) {

            if ($quotaTotal = $this->fertilizer_quota->where('year', $yearSelected)){
                $quotaTotal = $quotaTotal->sum('quota');
            }

            $result = $this->setDistributorTotalResult($yearSelected, $distributorSelected);

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Total Per Distributor',
                'fertilizerTypes' => $this->fertilizer_type,
                'results' => $result,
                'months' => $this->months,
                'distributors' => $this->distributor,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
                'yearSelected' => $yearSelected,
                'distributorSelected' => $distributorSelected,
                'quotaTotal' => $quotaTotal ? $quotaTotal : 0
            ];

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pupuk-bersubsidi.fertilizer-report.print-distributor-total', $data)
                ->setPaper('a4', 'landscape');
            return $pdf->stream();

        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param $distributorSelected
     * @param $yearSelected
     * @return array
     */
    private function setDistributorTotalResult($yearSelected, $distributorSelected)
    {
        $fertilizerRealizations = $this->fertilizer_realization
            ->where('year', $yearSelected)
            ->where('distributor_id', $distributorSelected);
        $fertilizerRealizationStocks = $this->fertilizer_realization_stock;

        $this->fertilizer_type->transform(function ($fertilizerType) use ($yearSelected) {
            $this->fertilizer_quota
                ->where('year', $yearSelected)
                ->transform(function ($fertilizerQuota) use ($fertilizerType) {
                    if ($fertilizerQuota->fertilizer_type_id == $fertilizerType->id) {
                        $fertilizerType->quota = $fertilizerQuota->quota;
                    }

                    return $fertilizerQuota;
                });

            return $fertilizerType;
        });

        $result = [];

        foreach($fertilizerRealizations as $fertilizerRealization) {
            foreach ($fertilizerRealizationStocks as $fertilizerRealizationStock) {
                if ($fertilizerRealization->id == $fertilizerRealizationStock->fertilizer_realization_id) {
                    if (!isset($result[$fertilizerRealization->month]
                        [$fertilizerRealizationStock->fertilizer_type_id])) {
                        $result[$fertilizerRealization->month]
                        [$fertilizerRealizationStock->fertilizer_type_id] = 0;
                    }

                    $result[$fertilizerRealization->month]
                    [$fertilizerRealizationStock->fertilizer_type_id] +=
                        $fertilizerRealizationStock->stock;
                }
            }
        }

        return $result;
    }
}