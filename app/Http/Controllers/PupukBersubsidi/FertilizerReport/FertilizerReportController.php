<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 02/11/16
 * Time: 17:47
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerReport;


use App\Http\Controllers\Controller;
use App\Http\Models\Distributor;
use App\Http\Models\District;
use App\Http\Models\FertilizerRealization;
use App\Http\Models\FertilizerRealizationStock;
use App\Http\Models\FertilizerType;
use App\Http\Models\Retailer;
use App\Http\Models\FertilizerQuota;
use App\Http\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FertilizerReportController extends Controller
{

    use FertilizerDistribution, \App\Http\Controllers\PupukBersubsidi\FertilizerReport\Distributor;

    /**
     * @var array
     */
    private $months = [
        ['id' => 1, 'name' => 'Januari'],
        ['id' => 2, 'name' => 'Februari'],
        ['id' => 3, 'name' => 'Maret'],
        ['id' => 4, 'name' => 'April'],
        ['id' => 5, 'name' => 'Mei'],
        ['id' => 6, 'name' => 'Juni'],
        ['id' => 7, 'name' => 'Juli'],
        ['id' => 8, 'name' => 'Agustus'],
        ['id' => 9, 'name' => 'September'],
        ['id' => 10, 'name' => 'Oktober'],
        ['id' => 11, 'name' => 'November'],
        ['id' => 12, 'name' => 'Desember'],
    ];

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $distributor, $retailer, $fertilizer_realization, $fertilizer_type,
        $fertilizer_realization_stock, $year, $district, $fertilizer_quota;

    /**
     * FertilizerReportController constructor.
     */
    public function __construct()
    {
        $this->distributor = Distributor::all();
        $this->retailer = Retailer::all();
        $this->fertilizer_realization = FertilizerRealization::all();
        $this->fertilizer_type = FertilizerType::all();
        $this->fertilizer_quota = FertilizerQuota::all();
        $this->fertilizer_realization_stock = FertilizerRealizationStock::all();
        $this->year = Year::all();
        $this->district = District::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {

        if ($year = $this->year->first()) {
            $monthId = $this->months[0]['id'];
            $yearSelected = date('Y');
            $distributorId = $this->distributor[0]->id;
            $fertilizerTypeId = $this->fertilizer_type[0]->id;

            if ($fertilizerRealization = $request->input()) {
                $monthId = 1; // it is not necessary though, lazy to refactor, hahaahaha
                $yearSelected = $fertilizerRealization['year'];
                $distributorId = $fertilizerRealization['distributor'];
                $fertilizerTypeId = $fertilizerRealization['fertilizerType'];
            }

            $getDistributorName = $this->getDistributionName($distributorId);

            $result = $this->setFertilizerReportResult($distributorId, $fertilizerTypeId, $monthId, $yearSelected);

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Tabel Realisasi Per Distributor Per Jenis Pupuk',
                'header' => $getDistributorName,
                'distributors' => $this->distributor,
                'retailers' => $this->retailer,
                'fertilizerTypes' => $this->fertilizer_type,
                'results' => $result,
                'distributorId' => $distributorId,
                'monthId' => $monthId,
                'fertilizerTypeId' => $fertilizerTypeId,
                'monthName' => $this->getMonthName($monthId),
                'months' => $this->months,
                'districts' => $this->district,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
                'yearSelected' => $yearSelected
            ];

            return view('pupuk-bersubsidi.fertilizer-report.distributor', $data);
        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param $distributorId
     * @param $fertilizerTypeId
     * @param $monthId
     * @param $yearSelected
     * @return \Illuminate\Http\RedirectResponse
     */
    public function printFertilizerReport($distributorId, $fertilizerTypeId, $monthId, $yearSelected)
    {
        if ($year = $this->year->first()) {
            $getDistributorName = $this->getDistributionName($distributorId);

            $result = $this->setFertilizerReportResult($distributorId, $fertilizerTypeId, $monthId, $yearSelected);

            $data = [
                'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Tabel Realisasi Per Distributor Per Jenis Pupuk',
                'header' => $getDistributorName,
                'distributors' => $this->distributor,
                'retailers' => $this->retailer,
                'fertilizerTypes' => $this->fertilizer_type,
                'results' => $result,
                'distributorId' => $distributorId,
                'monthId' => $monthId,
                'fertilizerTypeId' => $fertilizerTypeId,
                'monthName' => $this->getMonthName($monthId),
                'months' => $this->months,
                'districts' => $this->district,
                'startYear' => (int) $year->start_year,
                'endYear' => (int) $year->end_year,
                'yearSelected' => $yearSelected
            ];

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pupuk-bersubsidi.fertilizer-report.print-distributor', $data)
                ->setPaper('a4', 'landscape');
            return $pdf->stream();
        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param $name
     * @return int
     */
    private function getMonthId($name)
    {
        foreach ($this->months as $month) {
            if ($month['name'] == $name) {
                return $month['id'];
            }
        }

        return 0;
    }

    /**
     * @param $id
     * @return string
     */
    private function getDistributionName($id)
    {
        foreach ($this->distributor as $distributor) {
            if ($id == $distributor->id) {
                return $distributor->name;
            }
        }

        return '';
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getMonthName($id)
    {
        return $this->months[$id-1]['name'];
    }

    /**
     * @param $distributorId
     * @param $fertilizerTypeId
     * @param $monthId
     * @param $yearSelected
     * @return array
     */
    private function setFertilizerReportResult($distributorId, $fertilizerTypeId, $monthId, $yearSelected)
    {
        $fertilizerRealizationGroupBy = $this->fertilizer_realization
            ->where('distributor_id', $distributorId)->groupBy('distributor_id');
        $fertilizerRealizationStockGroupBy = $this->fertilizer_realization_stock
            ->where('fertilizer_type_id', $fertilizerTypeId)
            ->groupBy('fertilizer_realization_id');

        $result = [];

        foreach ($fertilizerRealizationGroupBy as $key => $items) {
            foreach ($items as $item) {
                if ($yearSelected == $item->year) {
                    if ($fertilizerRealizationStockGroupBy[$item->id]) {
                        foreach ($fertilizerRealizationStockGroupBy[$item->id] as $value) {
                            if (!isset($result[$item->retailer_id][$this->getMonthId($item->month)])) {
                                $result[$item->retailer_id][$this->getMonthId($item->month)] = 0;
                            }
                            $result[$item->retailer_id][$this->getMonthId($item->month)] += $value->stock;
                        }
                    }
                }
            }
        }

        return $result;
    }
}