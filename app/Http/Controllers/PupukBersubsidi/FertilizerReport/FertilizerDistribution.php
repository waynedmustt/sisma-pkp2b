<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 13/11/16
 * Time: 21:31
 */

namespace App\Http\Controllers\PupukBersubsidi\FertilizerReport;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

trait FertilizerDistribution
{
    /**
     * @param $year
     * @param $yearSelected
     * @return array
     */
    private function setFertilizerDistribution($year, $yearSelected)
    {
        $fertilizerRealizations = $this->fertilizer_realization
            ->where('year', $yearSelected);
        $fertilizerRealizationStocks = $this->fertilizer_realization_stock;

        $this->fertilizer_type->transform(function ($fertilizerType) use ($yearSelected) {
            $this->fertilizer_quota
                ->where('year', $yearSelected)
                ->transform(function ($fertilizerQuota) use ($fertilizerType) {
                    if ($fertilizerQuota->fertilizer_type_id == $fertilizerType->id) {
                        $fertilizerType->quota = $fertilizerQuota->quota;
                    }

                    return $fertilizerQuota;
                });

            return $fertilizerType;
        });

        $result = [];

        foreach($fertilizerRealizations as $fertilizerRealization) {
            foreach ($fertilizerRealizationStocks as $fertilizerRealizationStock) {
                if ($fertilizerRealization->id == $fertilizerRealizationStock->fertilizer_realization_id) {
                    if (!isset($result[$fertilizerRealizationStock->fertilizer_type_id]
                        [$fertilizerRealization->month])) {
                        $result[$fertilizerRealizationStock->fertilizer_type_id]
                        [$fertilizerRealization->month] = 0;
                    }

                    $result[$fertilizerRealizationStock->fertilizer_type_id]
                    [$fertilizerRealization->month] +=
                        $fertilizerRealizationStock->stock;
                }
            }
        }

        $data = [
            'title' => 'Informasi Realisasi Penyaluran Pupuk Bersubsidi - Penyaluran Pupuk Bersubsidi',
            'fertilizerTypes' => $this->fertilizer_type,
            'results' => $result,
            'months' => $this->months,
            'startYear' => (int) $year->start_year,
            'endYear' => (int) $year->end_year,
            'yearSelected' => $yearSelected
        ];

        return $data;
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getFertilizerDistribution(Request $request)
    {
        if ($year = $this->year->first()) {
            $yearSelected = date('Y');

            if ($fertilizerDistribution = $request->input()) {
                $yearSelected = $fertilizerDistribution['year'];
            }

            $data = $this->setFertilizerDistribution($year, $yearSelected);

            return view('pupuk-bersubsidi.fertilizer-report.fertilizer-distribution', $data);

        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }

    /**
     * @param $param
     * @return \Illuminate\Http\RedirectResponse
     */
    public function printFertilizerDistribution($param)
    {
        if ($year = $this->year->first()) {
            $yearSelected = $param;

            $data = $this->setFertilizerDistribution($year, $yearSelected);

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pupuk-bersubsidi.fertilizer-report.print-fertilizer-distribution', $data)->setPaper('a4', 'landscape');
            return $pdf->stream();
        }

        return redirect('pupuk-bersubsidi/fertilizer-realization')->with('failed' , 'Year has not been input!');
    }
}