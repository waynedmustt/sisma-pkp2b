<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/11/16
 * Time: 7:51
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class FertilizerQuota extends Model
{
    protected $table = 'fertilizer_quotas';
}