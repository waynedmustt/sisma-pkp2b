<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 21:09
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'villages';
}