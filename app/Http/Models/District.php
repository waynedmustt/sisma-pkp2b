<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:17
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    /**
     * @var string
     */
    protected $table = 'districts';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function survey_locations()
    {
        return $this->hasMany('App\Http\Models\SurveyLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function villages()
    {
        return $this->hasMany('App\Http\Models\Village');
    }
}