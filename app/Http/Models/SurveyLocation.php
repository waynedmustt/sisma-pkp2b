<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 31/10/16
 * Time: 17:46
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class SurveyLocation extends Model
{
    protected $table = 'survey_locations';
}