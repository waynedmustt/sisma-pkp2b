<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 21:43
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class FertilizerType extends Model
{
    protected $table = 'fertilizer_type';
}