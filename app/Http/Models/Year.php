<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/11/16
 * Time: 4:17
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'years';
}