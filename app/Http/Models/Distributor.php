<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 21:40
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    /**
     * @var string
     */
    protected $table = 'distributors';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function retailers()
    {
        return $this->hasMany('App\Http\Models\Retailer');
    }
}