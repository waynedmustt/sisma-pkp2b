<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 21:41
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    protected $table = 'retailers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\Http\Models\District');
    }
}