<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/11/16
 * Time: 7:02
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class CommodityStock extends Model
{
    /**
     * @var string
     */
    protected $table = 'commodity_stocks';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commodity()
    {
        return $this->belongsTo('App\Http\Models\commodity');
    }
}