<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:18
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class CommodityPrice extends Model
{
    /**
     * @var string
     */
    protected $table = 'commodity_prices';

}