<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 01/11/16
 * Time: 21:46
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class FertilizerRealization extends Model
{
    /**
     * @var string
     */
    protected $table = 'fertilizer_realizations';
}