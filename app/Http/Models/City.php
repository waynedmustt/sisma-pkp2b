<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:16
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * @var string
     */
    protected $table = 'cities';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function districts()
    {
        return $this->hasMany('App\Http\Models\District');
    }

}