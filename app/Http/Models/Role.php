<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 29/10/16
 * Time: 18:58
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * @var string
     */
    protected $table = 'role';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Http\Models\User');
    }

}