<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 10/11/16
 * Time: 8:24
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class FertilizerRealizationStock extends Model
{
    protected $table = 'fertilizer_realization_stocks';
}