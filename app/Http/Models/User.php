<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 29/10/16
 * Time: 18:57
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Http\Models\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function role_users()
    {
        return $this->hasMany('App\Http\Models\RoleUser');
    }
}