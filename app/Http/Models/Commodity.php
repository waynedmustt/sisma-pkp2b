<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 10:18
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $table = 'commodities';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function provinces()
    {
        return $this->belongsToMany('App\Http\Models\Province', 'commodity_prices');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commodity_prices()
    {
        return $this->hasMany('App\Http\Models\CommodityPrice');
    }
}