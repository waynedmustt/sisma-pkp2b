<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 29/10/16
 * Time: 18:58
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';
}