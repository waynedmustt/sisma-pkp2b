<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 30/10/16
 * Time: 5:41
 */

namespace App\Observers;


use App\Http\Models\RoleUser;
use App\Http\Models\User;

class UserObserver
{
    /**
     * if user was deleted, role user deleted also
     *
     * @param User $user
     */
    public function deleted(User $user){

        if ($role_user = RoleUser::where('user_id', $user->id)) {
            $role_user->delete();
        }

    }
}