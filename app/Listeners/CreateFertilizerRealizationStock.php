<?php

namespace App\Listeners;

use App\Events\FertilizerRealizationWasCreated;
use App\Http\Models\FertilizerRealizationStock;

class CreateFertilizerRealizationStock
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param FertilizerRealizationWasCreated $event
     */
    public function handle(FertilizerRealizationWasCreated $event)
    {

        $fertilizerRealization = new FertilizerRealizationStock;

        $fertilizerRealization->fertilizer_realization_id = $event->fertilizerRealizationId;
        $fertilizerRealization->fertilizer_type_id = $event->fertilizerTypeId;
        $fertilizerRealization->stock = $event->stock;

        $fertilizerRealization->save();
    }
}
