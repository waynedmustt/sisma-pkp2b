/**
 * Created by dmustt on 30/10/16.
 */

"use strict";

/**
 * JS root scopes
 * need to feed another js
 */
var _rootScope;


// initial dmustt apps module with config
var dmustt = angular.module('DmusttApps', []);

dmustt.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/**
 * Created by dmustt on 30/10/16.
 */

"use strict";

dmustt.controller('CommodityPriceFormController', [ '$scope', function ($scope) {

    $scope.province = '';
    $scope.provinces = [];
    $scope.city = '';
    $scope.cities = [];
    $scope.district = '';
    $scope.districts = [];
    $scope.surveyLocation = '';
    $scope.surveyLocations = [];
    $scope.commodity = '';
    $scope.commodities = [];
    $scope.commodityPrices = [];
    $scope.priceA = '';
    $scope.priceB = '';
    $scope.surveyDate = '';

    $scope.init = function (commodity, province, city, district, commodityPrices) {
        if (commodity === 'undefined' || commodity === null
            ||province === 'undefined' || province === null) {
            return;
        }

        if (commodityPrices !== '') {
            if (commodityPrices === 'undefined') {
                return;
            }

            angular.forEach(commodityPrices, function(value) {
                $scope.commodityPrices.push({
                    commodity : value.commodity,
                    province : value.province,
                    city : value.city,
                    district : value.district,
                    surveyLocation : value.surveyLocation,
                    priceA : value.priceA,
                    priceB : value.priceB,
                    surveyDate : value.surveyDate
                });
            });
        }

        $scope.provinces = province;
        $scope.cities = city;
        $scope.province = province[0];
        $scope.city = city[0];
        $scope.commodities = commodity;
        $scope.setDistrictByCityId(district);

    };

    $scope.removeCommodityPrice = function (index) {
        $scope.commodityPrices.splice(index, 1);
    };

    $scope.setTimeStampToServer = function (date) {
        if (date === 'undefined') {
            return;
        }

        var convertedDate = new Date(date),
        convertedMonth = convertedDate.getMonth() + 1;

        return convertedDate.getFullYear()
            + '-' + convertedMonth
            + '-' + convertedDate.getDate();
    };

    $scope.addCommodityPrice = function () {
        $scope.commodityPrices.push({
            commodity : $scope.commodity,
            province : $scope.province,
            city : $scope.city,
            district : $scope.district,
            surveyLocation : $scope.surveyLocation,
            priceA : $scope.priceA,
            priceB : $scope.priceB,
            surveyDate : $scope.setTimeStampToServer($scope.surveyDate)
        });
    };

    $scope.setCitiesEmpty = function () {
        $scope.cities = [];
    };

    $scope.setDistrictsEmpty = function () {
        $scope.districts = [];
    };

    $scope.setSurveyLocationsEmpty = function () {
        $scope.surveyLocations = [];
    };

    $scope.setCityByProvinceId = function (city) {
        $scope.setCitiesEmpty();

        if ($scope.provinces === '') {
            return;
        }

        angular.forEach(city, function(value) {
           if (value.province_id == $scope.province.id) {
                $scope.cities.push(value);
           }
        });
    };

    $scope.setDistrictByCityId = function (district) {
        $scope.setDistrictsEmpty();

        if ($scope.districts === '') {
            return;
        }

        angular.forEach(district, function(value) {
            if (value.city_id == $scope.city.id) {
                $scope.districts.push(value);
            }
        });
    };

    $scope.setSurveyLocationByDistrictId = function (surveyLocation) {
        $scope.setSurveyLocationsEmpty();

        if ($scope.surveyLocations === '') {
            return;
        }

        angular.forEach(surveyLocation, function(value) {
            if (value.district_id == $scope.district.id) {
                $scope.surveyLocations.push(value);
            }
        });
    };

}]);

/**
 * Created by dmustt on 02/11/16.
 */

"use strict";

dmustt.controller('FertilizerRealizationFormController', [ '$scope', function ($scope) {

    $scope.distributor = '';
    $scope.distributors = [];
    $scope.retailer = '';
    $scope.retailers = [];
    $scope.district = '';
    $scope.districts = [];
    $scope.village = '';
    $scope.villages = [];
    $scope.year = '';
    $scope.month = '';
    $scope.fertilizerRealizations = [];

    $scope.init = function (distributor, district) {
        if (distributor === 'undefined' || distributor === null ||
            district === 'undefined' || district === null) {
            return;
        }

        $scope.distributors = distributor;
        $scope.districts = district;

    }

    $scope.removeFertilizerRealization = function (index) {
        $scope.fertilizerRealizations.splice(index, 1);
    };

    $scope.addFertilizerRealization = function () {

        $scope.fertilizerRealizations.push({
            distributor : $scope.distributor,
            retailer : $scope.retailer,
            year : $scope.year,
            month : $scope.setMonthName($scope.month),
        });
    };

    $scope.setMonthName = function (month) {
        if (month === 'undefined') {
            return;
        }

        var months = [];
        months[0] = "Januari";
        months[1] = "Februari";
        months[2] = "Maret";
        months[3] = "April";
        months[4] = "Mei";
        months[5] = "Juni";
        months[6] = "Juli";
        months[7] = "Agustus";
        months[8] = "September";
        months[9] = "Oktober";
        months[10] = "November";
        months[11] = "Desember";

        return months[parseInt(month)-1];
    };

    $scope.setRetailersEmpty = function () {
        $scope.retailers = [];
    }

    $scope.setVillagesEmpty = function () {
        $scope.villages = [];
    }

    $scope.setRetailerByDistributorId = function (retailer) {
        $scope.setRetailersEmpty();

        if ($scope.retailers === '') {
            return;
        }

        angular.forEach(retailer, function(value) {
            if (value.distributor_id == $scope.distributor.id) {
                $scope.retailers.push(value);
            }
        });
    }

    $scope.setVillageByDistrictId = function (village) {
        $scope.setVillagesEmpty();

        if ($scope.villages === '') {
            return;
        }

        angular.forEach(village, function(value) {
            if (value.district_id == $scope.district.id) {
                $scope.villages.push(value);
            }
        });
    }

}]);

//# sourceMappingURL=dmustt.js.map
